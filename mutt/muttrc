# muttrc
# vi: ft=muttrc

# Account definitions
source ~/.mutt/muttrc-accounts
# My mail folders
source ~/.mutt/muttrc-folders

# Aliases
set alias_file=~/.mutt/aliases
source ~/.mutt/aliases

# GnuPG integration
source ~/.mutt/muttrc-gnupg

# My headers
source ~/.mutt/muttrc-headers

# My key bindings
source ~/.mutt/muttrc-keys

# Coloring
source ~/.mutt/muttrc-color

# MIME
source ~/.mutt/muttrc-mime

# Where to save SSL certificates (for (imaps|pops):// connections)
set certificate_file=~/.mutt/certificates
set header_cache=~/.cache/mutt
set message_cachedir=~/.cache/mutt

# Gvim doesn't work here
set editor="vim '+normal }'"

# Don't ask me if i want to append to mailboxes
set noconfirmappend
# Don't save empty files
set nosave_empty
# Always save a copy of outgoing messages
set copy
# Get message's envelope sender from the "From:" header
set envelope_from
# Don't move to the next message when the pager reaches the end
set pager_stop
# Pad blank lines in pager with ~ (ala vi)
set tilde

# Use a split pane to read mail
set pager_index_lines=12

# Always condense messages/replies into threads
set sort=threads

# Emulate Gmail's "last message in thread is thread date"
# with new threads toward the top
set sort_aux=reverse-last-date-received

# More business-like
set sig_on_top

# Menu title format (set to default)
#set status_format "-%r-Mutt: %f [Msgs:%?M?%M/?%m%?n? New:%n?%?o? Old:%o?%?d? Del:%d?%?F? Flag:%F?%?t? Tag:%t?%?p? Post:%p?%?b? Inc:%b?%?l? %l?]---(%s/%S)-%>-(%P)---"

# Layout
# (Default)
#set index_format="%4C %Z %{%b %d} %-15.15L (%4l) %s"
# Use chars (%4c) for message size, not lines
# ! = Convert to local time; without it, it uses the header time
#set index_format="%4C %Z %[!%b %d] %-15.15L (%4c) %s"
# Format relative date
set index_format="$HOME/.mutt/bin/reldate-index.sh %[%s] \"%4C %Z \" \" %-15.15L (%4c) %s\"|"
#set pager_format="-%Z- %C/%m: %-20.20n   %s%*  -- (%P)" # default
# Display local time in page underbar
set pager_format="-%Z- %C/%m: %[!%H:%M] %-20.20n   %s%*  -- (%P)"

# Random signature from signature.d
set signature="fortune ~/.mutt/signature.d;echo|"

set charset=latin1
#set send_charset="utf-8:us-ascii"

# SSL hardening
set ssl_force_tls=yes
set ssl_starttls=yes
set ssl_use_sslv2=no
set ssl_use_sslv3=no
set ssl_use_tlsv1=no
set ssl_use_tlsv1_1=no
set ssl_use_tlsv1_2=yes
set ssl_verify_dates=yes
set ssl_verify_host=yes
