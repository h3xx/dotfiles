#!/bin/bash
# vi: et sts=4 sw=4 ts=4

USAGE() {
    printf 'Usage: %s [OPTIONS] [--] FILE...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Preview the contents of Slackware packages.

  -h        Show this help message.
  --        Terminate options list.

Uses \$PAGER to page results.

Copyright (C) 2007-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

while getopts 'h' FLAG; do
    case "$FLAG" in
        'h')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

print_meta_files() {
    local \
        PKG_FILE=$1 \
        INSTALL_LIST \
        SLACK_META

    mapfile -t INSTALL_LIST < <(
        tar tf "$PKG_FILE" 'install' 2>/dev/null |
        grep -v '/$'
    )
    for SLACK_META in "${INSTALL_LIST[@]}"; do
        echo "==== $SLACK_META ===="
        tar xOf "$PKG_FILE" "$SLACK_META"
    done
}

for PKG_FILE; do
    if [[ -f $PKG_FILE ]]; then
        print_meta_files "$PKG_FILE" &&
        tar tvvf "$PKG_FILE"
    fi
done | ${PAGER:-less}
