#!/bin/bash
# vi: et sts=4 sw=4 ts=4
# Usefulness: *****

PACKAGES='/var/log/packages'
SCRIPTS='/var/log/scripts'

USAGE() {
    printf 'Usage: %s [--] PATTERN...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Slackware-specific script to display package contents.

  -h, --help    Show this help message.
  --            Terminate options list.

EOF
    COPYRIGHT_MESSAGE
}

COPYRIGHT_MESSAGE() {
    cat <<EOF
Copyright (C) 2007-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

ARGS=()
ARG_ERRORS=0
END_FLAGS=0
# Hint: $0 is the script name
for (( AI = 1; AI <= $#; ++AI )); do
    ARG=${!AI}
    if [[ $END_FLAGS -ne 0 ]]; then
        ARGS+=("$ARG")
    else
        case "$ARG" in
            --help|-h)
                HELP_MESSAGE
                exit 0
                ;;
            --)
                END_FLAGS=1
                ;;
            [^-]*)
                ARGS+=("$ARG")
                ;;
            *)
                printf '%s: Unrecognized option: %s\n' \
                    "${0##*/}" \
                    "$ARG" \
                    >&2
                ARG_ERRORS=1
        esac
    fi
done

if [[ ${#ARGS[@]} -eq 0 ]]; then
    USAGE
    ARG_ERRORS=1
fi

if [[ $ARG_ERRORS -ne 0 ]]; then
    printf 'Try '\''%s --help'\'' for more information.\n' \
        "${0##*/}" \
        >&2
    exit 2
fi

for PKG in $(chkpkg "${ARGS[@]}") ; do
    if [[ -f $SCRIPTS/$PKG ]]; then
        cat "$SCRIPTS/$PKG"
    fi
    cat "$PACKAGES/$PKG"
done
