#!/bin/bash
# vi: et sts=4 sw=4 ts=4

USAGE() {
    printf 'Usage: %s [OPTIONS] [FILE]... [DIR]...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Append doinst.sh information to install .info.gz files into /usr/info/dir.

  -h        Show this help message.
  -f        Force adding info files, even if they don't exist.
  -z        Automatically gzip(1) info files.
  -o DOINST Append to doinst.sh file DOINST.

Copyright (C) 2010-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

# should be used like so (from a build jail):
#
# $ rm usr/info/dir
# $ handy-info-install usr/info/*
#
# which will automatically create the `install' directory and append the
# information to `install/doinst.sh'

# --OR-- [since I'm always forgetting]
# $ rm usr/info/dir
# # cd install/
# $ handy-info-install ../usr/info/*
#
# will install the information into `./doinst.sh'

# note: will automatically remove usr/info/dir if passed as an argument

DEST_DOINST='install/doinst.sh'
USE_INFO_FILE_BASENAME=0
GZIP_INFOS=0
INFODIR='/usr/info' # Slackware default
FORCE=0

while getopts 'fho:z' FLAG; do
    case "$FLAG" in
        'f')
            FORCE=1
            ;;
        'o')
            DEST_DOINST=$OPTARG
            USE_INFO_FILE_BASENAME=1
            ;;
        'z')
            GZIP_INFOS=1
            ;;
        'h')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

compress_info() {
    local \
        INFOFILE=$1 \

    if [[ ${INFOFILE%.gz} = "$INFOFILE" ]]; then
        # not yet gzipped
        gzip -9 -- "$INFOFILE" || return 1
        INFOFILE+='.gz'
    fi
    echo "$INFOFILE"
}

# append the header for the info file install process
doinst_header() {
    local \
        DOINST=$1 \

    mkdir -p -- "$(dirname -- "$DOINST")" || return

    cat <<EOF >>"$DOINST"
if [ -x /usr/bin/install-info ]; then
EOF
}

doinst_entry() {
    local \
        DOINST=$1 \
        INFODIR=$2 \
        INFOFILE=$3 \

    cat <<EOF >>"$DOINST"
  install-info --info-dir=$INFODIR $INFOFILE 2>/dev/null
EOF
}

# append the footer for the info file install process
doinst_footer() {
    local \
        DOINST=$1 \

    cat <<EOF >>"$DOINST"
fi

EOF
}

INFOFILES=()
ERRORS=()
for ARG; do
    if [[ $FORCE -ne 0 || -f $ARG ]]; then
        INFOFILES+=("$ARG")
    else
        ERRORS+=("'$ARG' is not a file")
    fi
done

if [[ ${#ERRORS[@]} -gt 0 ]]; then
    for ERR in "${ERRORS[@]}"; do
        printf '%s\n' "$ERR" >&2
    done
    exit 1
fi

# Don't write broken "if" statements to doinst.sh!
if [[ ${#INFOFILES[@]} -gt 0 ]]; then

    doinst_header "$DEST_DOINST"
    for INFOFILE in "${INFOFILES[@]}"; do

        if [[ $GZIP_INFOS -ne 0 ]]; then
            INFOFILE=$(compress_info "$INFOFILE")
        fi

        # remove the `dir' file if it is passed to us
        if [[ $(basename -- "$INFOFILE" '.gz') = 'dir' ]]; then
            rm -fv -- "$INFOFILE"
        else
            # else continue as normal
            if [[ $USE_INFO_FILE_BASENAME -ne 0 ]]; then
                INFOFILE="usr/info/$(basename -- "$INFOFILE")"
            fi
            if [[ $(basename -- "$(dirname -- "$INFOFILE")") != 'info' ]]; then
                # info file is in a non-standard subdirectory
                INFOSUBDIR=$(basename -- "$(dirname -- "$INFOFILE")")
            else
                INFOSUBDIR=
            fi

            # install!
            doinst_entry \
                "$DEST_DOINST" \
                "$INFODIR${INFOSUBDIR:+/$INFOSUBDIR}" \
                "$INFOFILE"
        fi
    done
    doinst_footer "$DEST_DOINST"

fi
