#!/bin/bash
# vi: et sts=4 sw=4 ts=4

# Version 1.3

USAGE() {
    printf 'Usage: %s [OPTIONS] [--] [FILE.new]...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Merge incoming changes (/etc/*.new) to Slackware system config.

  -a        Automatically accept all changes. Potentially dangerous.
  -C        Don't use colordiff (default is to use it if available).
  -h        Show this help message.
  --        Terminate options list.

Copyright (C) 2015-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

SYSCONFDIRS=(
    /etc
    /usr/info
    # system-wide vimrc has been known to stage changes with a .new file
    /usr/share/vim
    /var/lib/games
    /var/yp
)

APPLY_ALL=0
USE_COLORDIFF=1

while getopts 'aC' FLAG; do
    case "$FLAG" in
        'a')
            APPLY_ALL=1
            ;;
        'C')
            USE_COLORDIFF=0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

# check to see if colordiff(1) is installed
if [[ $USE_COLORDIFF -ne 0 ]] &&
    ! type colordiff &>/dev/null; then
    USE_COLORDIFF=0
fi

DIFF_OPTS=(
    --side-by-side
    --report-identical-files
    --suppress-common-lines
    #--context=3
)

DIFFTOOL=vimdiff

report_change() {
    local -r \
        OLD_CONF=$1 \
        NEW_CONF=$2

    if [[ -f $OLD_CONF && -f $NEW_CONF ]]; then

        echo '*****'
        if [[ $USE_COLORDIFF -ne 0 ]]; then
            diff "${DIFF_OPTS[@]}" -- "$OLD_CONF" "$NEW_CONF" |colordiff
        else
            diff "${DIFF_OPTS[@]}" -- "$OLD_CONF" "$NEW_CONF"
        fi
        echo '*****'
    elif [[ -f $NEW_CONF ]]; then
        echo '*****'
        printf 'New file: "%s"\n' "$NEW_CONF"
        cat -v -- "$NEW_CONF"
        echo '*****'
    else
        printf 'Error finding changes for configuration file "%s" => "%s"\n' \
            "$OLD_CONF" \
            "$NEW_CONF" \
            >&2
    fi
}

accept_change() {
    local -r \
        OLD_CONF=$1 \
        NEW_CONF=$2

    if [[ -f $NEW_CONF ]]; then
        mv -v -- "$NEW_CONF" "$OLD_CONF" || exit 1
    else
        printf 'error applying changes for configuration file "%s" => "%s"\n' "$OLD_CONF" "$NEW_CONF" >&2
    fi
}

reject_change() {
    local -r \
        OLD_CONF=$1 \
        NEW_CONF=$2

    if [[ -f $NEW_CONF ]]; then
        rm -fv -- "$NEW_CONF" || exit 1
    else
        printf 'Couldn'\''t delete file "%s"\n' \
            "$NEW_CONF" \
            >&2
    fi
}

edit_change() {
    local -r \
        OLD_CONF=$1 \
        NEW_CONF=$2

    $DIFFTOOL "$OLD_CONF" "$NEW_CONF"
}

prompt_change() {
    local -r \
        OLD_CONF=$1 \
        NEW_CONF=$2
    local ANSWER

    # Show the user what they're accepting
    report_change "$OLD_CONF" "$NEW_CONF"

    if [[ $APPLY_ALL -ne 0 ]]; then
        # User wants to install all of them
        accept_change "$OLD_CONF" "$NEW_CONF"
    else
        read -r -p "$OLD_CONF => $NEW_CONF [(y)es/(N)o/(e)dit/(d)elete/(a)ll]? " ANSWER
        case "$ANSWER" in
            [Aa]*)
                APPLY_ALL=1
                accept_change "$OLD_CONF" "$NEW_CONF"
                ;;
            [Yy]*)
                accept_change "$OLD_CONF" "$NEW_CONF"
                ;;
            [Dd]*)
                reject_change "$OLD_CONF" "$NEW_CONF"
                ;;
            [Ee]*)
                edit_change "$OLD_CONF" "$NEW_CONF"
                prompt_change "$OLD_CONF" "$NEW_CONF"
                ;;

            #*)
                # do nothing...
        esac
    fi
}


if [[ $# -eq 1 ]]; then
    # We're being called from find(1) or on a single file

    for NCONF; do
        prompt_change "$(dirname -- "$NCONF")/$(basename -- "$NCONF" .new)" "$NCONF"
    done
else
    # We're scheduling

    # Send myself a message
    PASS_OPTS=()
    if [[ $APPLY_ALL -ne 0 ]]; then
        PASS_OPTS+=('-a')
    fi
    if [[ $USE_COLORDIFF -eq 0 ]]; then
        PASS_OPTS+=('-C')
    fi

    find "${SYSCONFDIRS[@]}" \
        -type f \
        -name '*.new' \
        -exec "$0" "${PASS_OPTS[@]}" {} \;
fi
