#!/bin/bash
# vi: et sts=4 sw=4 ts=4

shopt -s inherit_errexit
set -e

USAGE() {
    printf 'Usage: %s [OPTIONS]\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Open a web browser to create a new BitBucket pull request.

  -h            Show this help message.
  -s BRANCH     Set source branch (defaults to current branch).
  -t BRANCH     Set target branch (defaults to empty).

Copyright (C) 2015-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

ARG_SOURCE_BRANCH=
ARG_TARGET_BRANCH=

while getopts 'hs:t:' FLAG; do
    case "$FLAG" in
        's')
            ARG_SOURCE_BRANCH=$OPTARG
            ;;
        't')
            ARG_TARGET_BRANCH=$OPTARG
            ;;
        'h')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

_git_cfg() (
    # Prevent "git config" from exiting when the config value returned is empty
    git config "$@" || :
)

main() {
    SOURCE_BRANCH=$(source_branch)
    TARGET_BRANCH=$(target_branch)

    # construct URL
    PUSH_URL=$(push_url)
    if [[ -z $PUSH_URL ]]; then
        printf 'Unable to determine push URL (are you in a git directory?)\n'
        exit 2
    fi
    PULL_REQUEST_URL=$(pull_request_url)

    open_browser "$PULL_REQUEST_URL"
}

open_browser() {
    local URL=$1
    git web--browse "$URL"
}

pull_request_url() (
    HTTP_ARGS=(
        "source=$(url_encode "$SOURCE_BRANCH")"
    )
    if [[ -n $TARGET_BRANCH ]]; then
        HTTP_ARGS+=("dest=$(url_encode "$TARGET_BRANCH")")
    fi

    # Concatenate all args, URL encoding '&' to %26
    HTTP_PARAMS=$(IFS='&'; echo "${HTTP_ARGS[*]//&/%26}")

    HTTP_URL=$(echo "$PUSH_URL" |perl -p -e '
        s#^ssh://##;
        s#:\d+/#/#;
        if (m#^\w*@#) {
            s#:#/#g;
            s#^\w*@#https://#;
        }
        s#\.git$##;
    ')
    printf '%s/pull-requests/new?%s' \
        "$HTTP_URL" \
        "$HTTP_PARAMS"
)

push_url() (
    git remote get-url \
        --push \
        "$(_git_cfg "branch.$SOURCE_BRANCH.remote")"
)

ref_to_branch() {
    local REF=$1
    local IGNORE_FAIL=1
    if [[ $REF = '-' ]]; then
        IGNORE_FAIL=0
        REF='@{-1}'
    fi
    if [[ $IGNORE_FAIL -eq 1 ]]; then
        # rev-parse will fail, print $REF if it cannot find it locally
        # --> Don't fail; maybe this target branch only exists on the remote
        git rev-parse --abbrev-ref "$REF" 2>/dev/null || :
    else
        git rev-parse --abbrev-ref "$REF"
    fi
}

source_branch() {
    ref_to_branch "${ARG_SOURCE_BRANCH:-HEAD}"
}

target_branch() (
    if [[ -n $ARG_TARGET_BRANCH ]]; then
        ref_to_branch "$ARG_TARGET_BRANCH"
    fi
)

url_encode() {
    perl -MURI::Escape -e 'print &uri_escape($_), "\n" foreach @ARGV' "$@"
}

main "$@"
