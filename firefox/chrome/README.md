These files increase all devtools drag handle widths to 10px because I'm tired
of pixel-hunting just to resize the fucking toolbar.

These CSS files goes in the directory `<Firefox Profile>/chrome/`. For example:

- Linux:
  - `~/.mozilla/firefox/foobar.default/chrome/userChrome.css`
  - `~/.mozilla/firefox/foobar.default/chrome/userContent.css`
- Mac OSX:
  - `~/Library/Application Support/Firefox/Profiles/foobar.default/chrome/userChrome.css`
  - `~/Library/Application Support/Firefox/Profiles/foobar.default/chrome/userContent.css`

**Important:** `toolkit.legacyUserProfileCustomizations.stylesheets` needs to
be `true` in `about:config` in order to use this file!

...Calling it "legacy" is kinda funny since Firefox doesn't provide any other
way that I know of to do this!

- See: https://searchfox.org/mozilla-central/source/devtools/client/themes/splitters.css
- See: https://superuser.com/questions/1706966/is-it-possible-to-edit-the-stylesheet-of-firefox-developer-tools
- See: http://kb.mozillazine.org/UserChrome-example.css
- For debugging, see: https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox
