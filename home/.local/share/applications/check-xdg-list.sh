#!/bin/bash
# vi: et sts=4 sw=4 ts=4

# Check an XDG config file for system compatibility.
#
# Checks if .desktop files referenced in .list files (defaults.list) actually
# exist on the system.
#
# Copyright (C) 2023 Dan Church.
# License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
# with Commons Clause 1.0 (https://commonsclause.com/).
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.
# You may NOT use this software for commercial purposes.

WORKDIR=${0%/*}
DESKTOPDIRS=(
    "$WORKDIR"
    /usr/share/applications
)

LISTS=("$WORKDIR/defaults.list")
if [[ $# -ne 0 ]]; then
    LISTS=("$@")
fi

PROBLEMS=()
check_list() {
    local -r LIST=$1
    local -i LINENUM=0
    local \
        DESK \
        DESKTOPDIR \
        FOUND \
        LINE \

    while read -r LINE; do
        (( ++LINENUM ))

        # Strip comments, leading/trailing whitespace
        LINE=${LINE%%'#'*}
        LINE=${LINE##[[:space:]]}
        LINE=${LINE%%[[:space:]]}

        # Skip lines not matching "mime/type=program.desktop"
        if [[
            $LINE = '['* ||
            $LINE != *=*
        ]]; then
            continue
        fi
        DESK=${LINE#*=}
        FOUND=0
        for DESKTOPDIR in "${DESKTOPDIRS[@]}"; do
            if [[ -f $DESKTOPDIR/$DESK ]]; then
                FOUND=1
                continue
            fi
        done
        if [[ $FOUND -eq 0 ]]; then
            PROBLEMS+=("$LIST:$LINENUM: Not found: $DESK")
        fi
    done < "$LIST" || return
}

for FN in "${LISTS[@]}"; do
    check_list "$FN" || exit
done

if [[ ${#PROBLEMS[@]} -gt 0 ]]; then
    for PROBLEM in "${PROBLEMS[@]}"; do
        printf '%s\n' "$PROBLEM" >&2
    done
    exit 1
fi
