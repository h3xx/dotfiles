#!/usr/bin/env php
<?php

$script = array_shift($argv);

function HELP_MESSAGE($exit_code=0) {
    print "
Usage: {$script} [OPTIONS] [--] FILE...
       {$script} [OPTIONS] [--] -
Dump YAML files. '-' means STDIN.

  --help        Show this help message.
  -q            Quiet mode, syntax check only.
  -x            Export into valid PHP code.
  -j            Export into valid JSON code.
  -H, --with-filename
                Print the file name with output. This is the default when there
                  is more than one file dump.
  -h, --no-filename
                Suppress the prefixing of file names on output. This is the
                  default when there is only one file (or only standard input)
                  to dump.
  --            Terminate options list.

Copyright (C) 2015-2022 Dan Church.
Written by Dan Church
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
";
    exit($exit_code);
}

// parse arguments
$file_args = [];

$opts = [
    'format' => null,
    'filename' => null,
    'quiet' => false,
];
$flags_terminated = false;
foreach($argv as $value) {
    if ($flags_terminated || !preg_match(',^-,', $value)) {
        $file_args []= $value;
        continue;
    }

    switch ($value) {
    case '-':
        $file_args []= '/dev/stdin';
        break;
    case '-H':
        $opts['filename'] = true;
        break;
    case '-h':
        $opts['filename'] = false;
        break;
    case '--help':
        HELP_MESSAGE(0);
    case '-x':
        $opts['format'] = 'export';
        break;
    case '-j':
        $opts['format'] = 'json';
        break;
    case '-q':
        $opts['quiet'] = true;
        break;
    case '--':
        $flags_terminated = true;
        break;
    default:
        fprintf(STDERR, "Unrecognized option: $value\n");
        HELP_MESSAGE(1);
    }

}

// print filename if more than one file being dumped
if (!isset($opts['filename'])) {
    $opts['filename'] = count($file_args) > 1;
}

// support "- !include FILE" directives
$ndocs = 0;
$callbacks = [
    '!include' => null,
];

$parse_errors = false;

// begin output
foreach ($file_args as $file) {
    $callbacks['!include'] =
        function ($value, $tag, $flags)
        use ($file, &$ndocs, &$callbacks, &$parse_errors) {
            $inc_file = dirname($file)."/$value";
            $inc_data = yaml_parse_file($inc_file, 0, $ndocs, $callbacks);
            if ($inc_data === false) {
                fprintf(STDERR, "Failed to parse file `$inc_file' included from `$file'\n");
                $parse_errors = true;
            }
            return $inc_data;
        };

    $data = yaml_parse_file($file, 0, $ndocs, $callbacks);

    if ($data === false) {
        fprintf(STDERR, "Failed to parse file `$file'\n");
        $parse_errors = true;
    } elseif (!$opts['quiet']) {
        switch ($opts['format']) {
            case 'export':
                if ($opts['filename']) {
                    print "### {$file}\n\n";
                }
                var_export($data);
                break;
            case 'json':
                if ($opts['filename']) {
                    print "// {$file}\n\n";
                }
                print json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                break;
            default:
                if ($opts['filename']) {
                    print "### {$file}\n\n";
                }
                print_r($data);
                break;
        }
        print "\n";
    }
}

if ($parse_errors) {
    exit(2);
}
