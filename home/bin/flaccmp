#!/bin/bash
# vi: et sts=4 sw=4 ts=4

USAGE() {
    printf 'Usage: %s [OPTIONS] [--] FILE1 FILE2\n' \
        "${0##*/}" \
        >&2
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Compare two FLAC files

  -h        Show this help message.

Copyright (C) 2017-2023 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

while getopts 'h' FLAG; do
    case "$FLAG" in
        'h')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

if [[ $# -ne 2 ]]; then
    USAGE >&2
    exit 2
fi

TEMP_DIR=$(mktemp -d -t "${0##*/}.XXXXXX")
cleanup() {
    rm -fr -- "$TEMP_DIR"
}
trap 'cleanup' EXIT

compare_md5_in_streaminfo() {
    local -r \
        FILE_A=$1 \
        FILE_B=$2
    [[ "$(metaflac --show-md5sum "$FILE_A")" = "$(metaflac --show-md5sum "$FILE_B")" ]]
}

compare_wav_contents() {
    local -r \
        FILE_A=$1 \
        FILE_B=$2
    local -r \
        WAV_TEMP_A=$(mktemp -p "$TEMP_DIR") \
        WAV_TEMP_B=$(mktemp -p "$TEMP_DIR")
    flac -d -f -o "$WAV_TEMP_A" "$FILE_A" 2>/dev/null &&
    flac -d -f -o "$WAV_TEMP_B" "$FILE_B" 2>/dev/null &&
    cmp "$WAV_TEMP_A" "$WAV_TEMP_B"
}

compare_md5_in_streaminfo "$1" "$2" &&
compare_wav_contents "$1" "$2"
