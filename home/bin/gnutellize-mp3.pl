#!/usr/bin/perl
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;
our $VERSION = 0.09;

sub HELP_MESSAGE {
    print <<"EOF";
Usage: $0 FILES
Transform filenames of audio files so they contain all relevant metadata.

  -v            Verbose output.
  -f            Force overwrite of renamed files.
  -D            Dry run; don't perform any actions.
  -a ARTIST     Assume the album artist is ARTIST.
  -M            Assume MP3.
  -F            Assume FLAC.
  -O            Assume OGG.
  -l LAYOUT     Set the filename layout. It's recommended you select from the
                  following fields:
                  - TITLE
                  - ARTIST
                  - ALBUM_ARTIST
                  - ALBUM
                  - DISCNUMBER
                  - DATE
                  - TRACKNUMBER
                  - TRACKTOTAL
                  - GENRE
                  - DESCRIPTION (i.e. the comment)
                  - COMPOSER
                  - PERFORMER
                  - COPYRIGHT
                  - LICENSE
                  - ENCODED-BY
                  Defaults to 'ALBUM_ARTIST ALBUM DISCNUMBER DATE TRACKNUMBER TITLE'.
  -C            Use compilation mode. Equivalent to
                  "-l 'ALBUM DATE TRACKNUMBER ARTIST TITLE'"
  -s STRING     Use STRING as the separator between fields. Defaults to " - ".

Copyright (C) 2010-2023 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
    return;
}

use Carp qw/ croak /;
use Cwd qw/ abs_path /;
use File::Basename qw/ dirname /;
require File::LibMagic;
use File::Path qw/ make_path /;
use Getopt::Long qw/ GetOptions :config bundling no_getopt_compat no_ignore_case /;

my (
    $help,
    $opt_assume_format,
    $opt_dry_run,
    $opt_force_overwrite,
    $opt_layout,
    $opt_override_artist,
    $opt_separator,
    $opt_verbose,
);
$opt_layout = 'ALBUM_ARTIST ALBUM DISCNUMBER DATE TRACKNUMBER TITLE';
unless (GetOptions(
    'C' => sub { $opt_layout = 'ALBUM DISCNUMBER DATE TRACKNUMBER ARTIST TITLE' },
    'D' => \$opt_dry_run,
    'F' => sub { $opt_assume_format = 'flac' },
    'M' => sub { $opt_assume_format = 'mp3' },
    'O' => sub { $opt_assume_format = 'ogg' },
    'a=s' => \$opt_override_artist,
    'f' => \$opt_force_overwrite,
    'l=s' => \$opt_layout,
    'help|h' => \$help,
    's=s' => \$opt_separator,
    'v' => \$opt_verbose,
)) {
    exit 2;
}

if ($help) {
    HELP_MESSAGE();
    exit 0;
}

# Instructions on how to format the different parts of the tag when used in a
# filename. Default is '%s'.
my %tagformats = (
    DATE => '(%04d)',
    DISCNUMBER => '(Disc %d)',
    TRACKNUMBER => sub {
        # if number, %02d, else %2s
        $_[0] =~ /\D/ ? '%2s' : '%02d'
    },
    TRACKTOTAL => 'of %02d',
);

my $flm = File::LibMagic->new;

sub _taghash {
    my $filename = abs_path(shift);

    if (-e $filename) {
        if (defined $opt_assume_format) {
            if ($opt_assume_format eq 'mp3') {
                return _mp3_taghash($filename);
            }
            if ($opt_assume_format eq 'flac') {
                return _flac_taghash($filename);
            }
            if ($opt_assume_format eq 'ogg') {
                return _ogg_taghash($filename);
            }
        }

        # checktype_* will return the `charset=' attribute as well.
        # For instance: `audio/mpeg; charset=binary'
        (my $mime_short = $flm->checktype_filename($filename))
            =~ s/;.*$//;

        if ($mime_short =~ m{audio/mpeg}) {
            return _mp3_taghash($filename);
        }
        if ($mime_short =~ m{audio/(?:x-)?flac}) {
            return _flac_taghash($filename);
        }
        if ($mime_short =~ m{(application|audio)/ogg}) {
            return _ogg_taghash($filename);
        }
        say STDERR "Unknown mimetype for file `$filename': $mime_short";
    }
    return;
}

sub _mp3_taghash {
    my $filename = shift;

    require MP3::Tag;
    (my $mp3 = MP3::Tag->new($filename))
        ->get_tags;

    # Make sure the library is reading the original performer's and
    # the composers' names right (also prevents them from being
    # superceded by the 'artist' tag value)
    $mp3->config('performer', 'TOPE');
    $mp3->config('composer', 'TCOM');

    # Transform into a Vorbis-tag-styled hash
    my %tags = (
        'TITLE'         => ($mp3->title)[0] || undef,
        'ARTIST'        => ($mp3->artist)[0] || undef,
        'ALBUM_ARTIST'  => ($mp3->get_id3v2_frames('TPE2'))[1] || undef,
        'ALBUM'         => ($mp3->album)[0] || undef,
        'DISCNUMBER'    => ($mp3->get_id3v2_frames('TPOS'))[1] || undef,
        'DATE'          => ($mp3->year)[0] || undef,
        'TRACKNUMBER'   => (split /\//, $mp3->track)[0] || undef,
        'TRACKTOTAL'    => (split /\//, $mp3->track)[1] || undef,
        'GENRE'         => ($mp3->genre)[0] || undef,
        'DESCRIPTION'   => ($mp3->comment)[0] || undef,
        'COMPOSER'      => ($mp3->composer)[0] || undef,
        'PERFORMER'     => ($mp3->performer)[0] || undef,
        'COPYRIGHT'     => ((($mp3->get_id3v2_frames('TCOP'))[1] || '') =~ /^\(C\) (.*)$/)[0] || undef,
        'LICENSE'       => (($mp3->get_id3v2_frames('WXXX'))[1] || {})->{URL} || undef,
        'ENCODED-BY'    => ($mp3->get_id3v2_frames('TENC'))[1] || undef,
    );

    return %tags;
}

sub _flac_taghash {
    my $filename = shift;

    require Audio::FLAC::Header;
    my $flac = Audio::FLAC::Header->new($filename);

    # Normalize tags
    # If this seems too easy, I don't know what to tell you.
    # TODO Vorbis tags allow multiple values with the same tag name, and tags
    # are only case-insensitive on a per-implementation basis.
    my %tags;
    while (my ($key, $val) = each %{ $flac->tags }) {
        $tags{ (uc $key) } = $val;
    }
    return %tags;
}

sub _ogg_taghash {
    my $filename = shift;

    require Ogg::Vorbis;

    my $get_tags = sub {
        my $fh = shift;
        my $ogg = Ogg::Vorbis->new;
        $ogg->open($fh);
        return %{ $ogg->comment };
    };

    open my $ifh, '<', $filename
        or croak("Cannot open file `$filename' for reading: $!");
    my %raw_tags = $get_tags->($ifh);
    close $ifh;

    # Normalize tags
    # TODO Vorbis tags allow multiple values with the same tag name, and tags
    # are only case-insensitive on a per-implementation basis.
    my %tags;
    while (my ($key, $val) = each %raw_tags) {
        $tags{ (uc $key) } = $val;
    }
    return %tags;
}

sub _tag_filename {
    my ($filename, $layout, $separator) = @_;

    my %tags = _taghash($filename);

    # Eliminate ` (Disc \d)' from the album title if we have DISCNUMBER
    if (defined $tags{DISCNUMBER}) {
        $tags{ALBUM} =~ s/\s*[(\[]?Disc\s*\d*.*[\])]?\s*$//;
    }

    $tags{ALBUM_ARTIST} //= $tags{ARTIST};

    if (defined $opt_override_artist) {
        # User specified overriding album artist
        $tags{ARTIST} = $opt_override_artist;
        $tags{ALBUM_ARTIST} = $opt_override_artist;
    }

    my (@patterns, @parts);

    foreach my $tagname (@{$layout}) {

        if (defined $tags{$tagname} and length $tags{$tagname}) {
            my $selected_format = $tagformats{$tagname} // '%s';
            if (ref $selected_format eq 'CODE') {
                $selected_format = $selected_format->($tags{$tagname});
            }
            push @patterns, $selected_format;
            push @parts, $tags{$tagname};
        }
    }

    unless (@patterns) {
        say STDERR "Warning: Not enough tag information for file `$filename'";
        return $filename;
    }

    # Make sure we don't have directory separators in the name
    foreach my $part (@parts) {
        $part =~ s{/}{--}g;
    }

    my $fn_pat = join $separator, @patterns;
    my $extension = (split /\./, $filename)[-1];
    return (sprintf $fn_pat, @parts) . ".$extension";
}

my @layout = split /\s+/, $opt_layout;
my $separator = $opt_separator // ' - ';

foreach my $filename (@ARGV) {
    my $new_filename = _tag_filename($filename, \@layout, $separator);

    next if $filename eq $new_filename;

    if (-e $new_filename && !$opt_force_overwrite) {
        say STDERR 'File exists, skipping.';
        next;
    }

    if ($opt_dry_run || $opt_verbose) {
        say STDERR "`$filename' => `$new_filename'";
    }
    unless ($opt_dry_run) {
        make_path(dirname($new_filename));
        rename $filename, $new_filename
            or croak("Failed to rename `$filename' -> `$new_filename': $!");
    }
}
