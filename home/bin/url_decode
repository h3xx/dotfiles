#!/usr/bin/perl
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;

our $VERSION = '1.0.2';

=head1 NAME

url_decode - Encode/decode URL strings

=head1 SYNOPSIS

B<url_decode> [I<OPTION>]... I<URL>...
B<url_encode> [I<OPTION>]... I<URL>...

=head1 OPTIONS

=over 4

=item -i I<FILE>

Take input from I<FILE>

=item --help

Display this help and exit.

=back

=head1 AUTHOR

Dan Church S<E<lt>h3xx@gmx.comE<gt>>

=head1 LICENSE AND COPYRIGHT

Copyright (C) 2026-2023 Dan Church.

License GPLv3: GNU GPL version 3.0 (L<https://www.gnu.org/licenses/gpl-3.0.html>)
with Commons Clause 1.0 (L<https://commonsclause.com/>).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.

=cut

use Carp qw/ croak /;
use Getopt::Long qw/ GetOptions :config bundling no_getopt_compat no_ignore_case /;
use File::Basename qw/ basename /;
use URI::Escape qw/ uri_escape uri_unescape /;
# Optional requirement: Pod::Usage
if (defined eval { require Pod::Usage }) {
    Pod::Usage->import(qw/ pod2usage /);
} else {
    sub pod2usage {
        my %args = @_;
        say STDERR "Try 'perldoc $0' for more information";
        if (defined $args{-exitval}) {
            exit $args{-exitval}
        }
    }
}

my (
    $opt_input,
    $help,
);
unless (GetOptions(
    'i=s' => \$opt_input,
    'help' => \$help,
)) {
    pod2usage(
       -exitval => 2,
       -msg => "Try '$0 --help' for more information",
    );
}

if ($help) {
    pod2usage(
        -verbose => 1,
        -exitval => 0,
    );
}

# Print usage message if no arguments specified
unless ($opt_input || @ARGV) {
    pod2usage(
        -exitval => 1,
        -sections => 'SYNOPSIS',
        -verbose => 0,
    );
}

my %transforms = (
    url_decode => \&uri_unescape,
    url_encode => \&uri_escape,
);

my $transform_op = $transforms{(basename($0))};

unless (defined $transform_op) {
    croak("Cannot determine operation for program name `$0'");
}

if ($opt_input) {
    my $fh;
    if ($opt_input eq '-') {
        $fh = *STDIN;
    } else {
        open $fh, '<', $opt_input
            or croak("Failed to open file `$opt_input' for reading: $!");
    }

    while (my $url = <$fh>) {
        chomp $url;
        say $transform_op->($url);
    }
    close $fh;
}

foreach my $url (@ARGV) {
    say $transform_op->($url);
}
