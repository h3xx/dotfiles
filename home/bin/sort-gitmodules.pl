#!/usr/bin/perl
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;

=head1 NAME

sort-gitmodules.pl - Sort Git module files

=head1 SYNOPSIS

B<sort-gitmodules.pl> < I<FILE1> > I<FILE2>

B<sort-gitmodules.pl> < .gitmodules > .gitmodules.sorted

=head1 COPYRIGHT

Copyright (C) 2020-2022 Dan Church S<E<lt>amphetamachine@gmail.comE<gt>>

License GPLv3: GNU GPL version 3.0 (L<https://www.gnu.org/licenses/gpl-3.0.html>)

with Commons Clause 1.0 (L<https://commonsclause.com/>).

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.

You may NOT use this software for commercial purposes.

=cut

MAIN: {
    my $gm = Git::Modules->new_from_text(<>);
    print $gm->unique;
}

package Git::Modules;
use 5.012;
use warnings;
use overload '""' => '_as_string';

sub new {
    my ($class, %args) = @_;
    return bless {
        ents => [],
        %args,
    }, $class;
}

sub new_from_text {
    my ($class, @lines) = @_;
    my $self = $class->new;
    $self->add_from_text(@lines);
    return $self;
}

sub add_from_text {
    my ($self, @lines) = @_;
    @lines = map { split /\n/ } @lines;
    my @curr_lines;
    foreach my $line (@lines) {
        if ($line =~ /^\[submodule\s+".*"\]$/) {
            if (@curr_lines) {
                push @{$self->{ents}}, Git::Modules::Entry->new_from_text(@curr_lines);
                # Start over
                @curr_lines = ();
            }
        }
        push @curr_lines, $line;
    }
    if (@curr_lines) {
        push @{$self->{ents}}, Git::Modules::Entry->new_from_text(@curr_lines);
    }
    return;
}

sub unique {
    my $self = shift;
    my %new_ents = map {
        ($_->{name}, $_)
    } @{$self->{ents}};
    return Git::Modules->new(
        ents => [values %new_ents],
    );
}

sub _as_string {
    my $self = shift;
    # No header, just entries
    return (join "\n", (
        sort {
            $a->{name} cmp $b->{name}
        } @{$self->{ents}}
    )) . "\n";
}

package Git::Modules::Entry;
use 5.012;
use warnings;
use overload '""' => '_as_string';

use fields qw/
    attrs
    name
/;

use Carp qw/ croak /;

sub new {
    my ($class, %args) = @_;
    my Git::Modules::Entry $self = fields::new($class);
    %{$self} = (%{$self}, %args);
    return $self;
}

sub new_from_text {
    my ($class, @lines) = @_;
    @lines = map { split /\n/ } @lines;

    my $found_head = 0;
    my %attrs;
    my $name;
    foreach my $line (@lines) {
        # Ignore blank lines
        if ($line =~ /^\s*$/) {
            next;
        }

        if ($line =~ /^\[submodule\s+"(.*)"\]$/) {
            $name = $1;
            if ($found_head) {
                croak("Tried to begin a new entry (name=$name)");
            }
            $found_head = 1;
        } elsif ($line =~ /^\s*([^=]+?)\s*=\s*(.*)$/) {
            $attrs{$1} = $2;
        } else {
            croak("Unrecognized line: $line");
        }
    }

    return __PACKAGE__->new(
        attrs => \%attrs,
        name => $name,
    );
}

sub _as_string {
    my $self = shift;
    # TODO what if there's a quote

    # Put these keys first, in this order
    my @keys_first = qw/ path url branch /;
    my %used_keys;
    @used_keys{@keys_first} = (1) x @keys_first;
    my @rest_of_keys = grep {
        !$used_keys{$_}
    } keys %{$self->{attrs}};
    #@used_keys{@rest_of_keys} = (1) x @rest_of_keys;
    my @keys = (
        @keys_first,
        (sort @rest_of_keys),
    );

    return (sprintf "[submodule \"%s\"]\n", $self->{name}) .
    (join "\n",
        map {
            sprintf "\t%s = %s", $_, $self->{attrs}->{$_}
        } grep {
            defined $self->{attrs}->{$_}
        } @keys
    );
}
