#!/bin/bash
# vi: et sts=4 sw=4 ts=4

USAGE() {
    printf 'Usage: %s [-n] TARBALL-OR-DIR TARBALL-OR-DIR\n' \
        "${0##*/}"
}

NEW_FILES=0
while getopts 'n' FLAG; do
    case "$FLAG" in
        'n')
            NEW_FILES=1
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

TEMP_DIR=$(mktemp -d -t "${0##*/}.XXXXXX")
cleanup() {
    rm -rf -- "$TEMP_DIR"
}

trap 'cleanup' EXIT

diemsg() {
    echo "${*:-died}"
    cleanup
    exit 1
}

tar_maindir() (
    FIRSTENTRY=$(tar tf "$1" 2>/dev/null |sort |head -1)

    printf '%s\n' \
        "${FIRSTENTRY%%/*}"
)

diff_orig() {
    local \
        SRC1=${1%/} \
        SRC2=${2%/} \
        TEMP1 \
        TEMP2 \
        DIR1 \
        DIR2 \
        DIFF_OPTS=(
            '-ru'
        )

    if [[ $NEW_FILES -ne 0 ]]; then
        DIFF_OPTS+=(
            # treat absent files as empty
            '-N'
        )
    fi

    # set up signal handlers
    trap 'diemsg "Killed"' INT

    if [[ -f $SRC1 ]]; then
        # Attempt to untar the source file to a temporary directory
        TEMP1=$(mktemp -d -p "$TEMP_DIR")
        if ! tar x -C "$TEMP1" -f "$SRC1"; then
            diemsg "Failed to un-tar file \`$SRC1' into \`$TEMP1"
        fi
        DIR1=$TEMP1/$(tar_maindir "$SRC1")
    else
        DIR1="$SRC1"
    fi

    if [[ -f ${SRC2:=.} ]]; then
        # Attempt to untar the source file to a temporary directory
        TEMP2=$(mktemp -d -p "$TEMP_DIR")
        if ! tar x -C "$TEMP2" -f "$SRC2"; then
            diemsg "Failed to un-tar file \`$SRC2' into \`$TEMP2"
        fi
        DIR2=$TEMP2/$(tar_maindir "$SRC2")
    else
        DIR2=$SRC2
    fi

    diff "${DIFF_OPTS[@]}" "$DIR1" "$DIR2" |
    grep -Ev '^(Only|Binary)' |
    perl -e '
my ($pat, $rep) = (quotemeta shift, shift);

while(<>) {
    s/$pat/$rep/g
        if m#^(diff|---)#;
    print
}' "$DIR1" "$DIR2.orig"
}

diff_orig "$@"
