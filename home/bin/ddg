#!/bin/bash
# vi: et sts=4 sw=4 ts=4
# opens elinks to the specified (or un-specified) search query

if [[ -z ${BROWSER:=$(type -afp elinks links lynx curl |head -1)} ]]; then
    echo 'No suitable text mode browser found.' >&2
    echo 'Set BROWSER environment variable to specify one manually.' >&2
    exit 2
fi

USAGE() {
    printf 'Usage: %s [OPTIONS] [QUERY]...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<'EOF'
Search DuckDuckGo

Options:
  -d            Use as a dictionary by adding `define:' before the search
                  term.
  -e KEY=VALUE  Manually append a URL parameter.
  -h            Show this help message.
  -q            Use exact phrase matching by surrounding QUERY with double
                quotes. Has the same effect as `-f search -v as_epq'.
  -s DOMAIN     Limit search results to pages within DOMAIN by prefixing QUERY
                  with `site:DOMAIN '.
  -S            Use safe search.
  -v STRING     Use STRING as the query parameter key name. Default is `q'.

You can manually specify the browser the link opens with by setting the
`BROWSER' environment variable.

For more information, see https://duckduckgo.com/params.html

Copyright (C) 2010-2022 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

QUERY_PREFIX=
QUERY_VAR='q'
EXTRA_ARGS=()
QUOTE=0
#ENCODING='UTF-8' # TODO : add as option
while getopts 'ds:qSe:v:h-' FLAG; do
    case "$FLAG" in
        'd')
            QUERY_PREFIX='define:'
            QUERY_VAR='q'
            ;;
        's')
            QUERY_PREFIX="site:$OPTARG "
            QUERY_VAR='q'
            ;;
        'q')
            QUOTE=1
            ;;
        'S')
            EXTRA_ARGS+=('kp=1')
            ;;
        'e')
            EXTRA_ARGS+=("$OPTARG")
            ;;
        'v')
            QUERY_VAR=$OPTARG
            ;;
        'h'|'-')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

url_encode() {
    perl -MURI::Escape -e 'print &uri_escape($_), "\n" foreach @ARGV' "$@"
}

collate_args() {
    local -r ARGS=$(IFS=; echo "${EXTRA_ARGS[*]/%/&}")
    echo -n "${ARGS%&}"
}

# Set the encoding
#EXTRA_ARGS+=("ie=$ENCODING")

QUERY=$*

if [[ $QUOTE -ne 0 ]]; then
    QUERY="\"$QUERY\""
fi

EXTRA_ARGS+=("$QUERY_VAR=$(url_encode "$QUERY_PREFIX$QUERY")")

# Add browser name to args
if [[ ${EXTRA_ARGS[*]#client=} = "${EXTRA_ARGS[*]}" ]]; then
    EXTRA_ARGS+=("client=$(url_encode "$(basename "$BROWSER")")")
fi

URL="https://duckduckgo.com/?$(collate_args)"

exec $BROWSER "$URL"
