#!/usr/bin/perl
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;

=head1 NAME

sort-gitconfig.pl - Sort Git config files

=head1 SYNOPSIS

B<sort-gitconfig.pl> < I<FILE1> > I<FILE2>

B<sort-gitconfig.pl> < ~/.config/git/config > ~/.config/git/config.sorted

=head1 COPYRIGHT

Copyright (C) 2021-2022 Dan Church S<E<lt>amphetamachine@gmail.comE<gt>>

License GPLv3: GNU GPL version 3.0 (L<https://www.gnu.org/licenses/gpl-3.0.html>)

with Commons Clause 1.0 (L<https://commonsclause.com/>).

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.

You may NOT use this software for commercial purposes.

=cut

MAIN: {
    my $gc = Git::Config->new_from_text(<>);
    print $gc->unique;
}

package Git::Config;
use 5.012;
use warnings;
use overload '""' => '_as_string';

sub new {
    my ($class, %args) = @_;
    return bless {
        ents => [],
        %args,
    }, $class;
}

sub new_from_text {
    my ($class, @lines) = @_;
    my $self = new($class);
    $self->add_from_text(@lines);
    return $self;
}

sub add_from_text {
    my ($self, @lines) = @_;
    @lines = map { split /\n/ } @lines;
    my @curr_lines;
    foreach my $line (@lines) {
        if ($line =~ /^\S/) {
            if (@curr_lines) {
                push @{$self->{ents}}, Git::Config::Section->new_from_text(@curr_lines);
                # Start over
                @curr_lines = ();
            }
        }
        push @curr_lines, $line;
    }
    if (@curr_lines) {
        push @{$self->{ents}}, Git::Config::Section->new_from_text(@curr_lines);
    }
    return;
}

sub unique {
    my $self = shift;
    my %new_ents = map {
        ($_->{name}, $_)
    } @{$self->{ents}};
    return __PACKAGE__->new(
        ents => [values %new_ents],
    );
}

sub _as_string {
    my $self = shift;
    # No header, just entries
    return (join "\n\n",
        sort {
            $a->{name} cmp $b->{name}
        } @{$self->{ents}}
    ) . "\n";
}

package Git::Config::Section;
use 5.012;
use warnings;
use overload '""' => '_as_string';

use fields qw/
    name
    lines
/;

use Carp qw/ croak /;

sub new {
    my ($class, %args) = @_;
    my Git::Config::Section $self = fields::new($class);
    %{$self} = (%{$self}, %args);
    return $self;
}

sub new_from_text {
    my ($class, @lines) = @_;
    @lines = map { split /\n/ } @lines;

    my @sub_lines;
    my $name = '';
    foreach my $line (@lines) {
        # Ignore blank lines
        if ($line =~ /^\s*$/) {
            next;
        }

        if ($line =~ /^\[(.*)\]$/) {
            #croak "Tried to begin a new entry (name=$1)" if defined $name;
            if ($name) {
                croak("Tried to begin a new entry (name=$1)\n" . (join "\n", @lines));
            }
            $name = $1;
        } else {
            push @sub_lines, $line;
        }
    }

    return __PACKAGE__->new(
        lines => \@sub_lines,
        name => $name,
    );
}

sub _as_string {
    my $self = shift;
    my $out = '';
    if ($self->{name}) {
        $out .= sprintf "[%s]\n", $self->{name};
    }
    return $out . (join "\n", @{$self->{lines}});
}
