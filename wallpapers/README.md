## Image Credits

| Wallpaper | Author | License
| - | - | -
| [cities/city-2kJsaMK.jpg](cities/city-2kJsaMK.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-3yX4Lee.jpg](cities/city-3yX4Lee.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-4FuEHWn.jpg](cities/city-4FuEHWn.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-8ByM2oB.jpg](cities/city-8ByM2oB.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-I1gzMyI.jpg](cities/city-I1gzMyI.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-XAUGaxL.jpg](cities/city-XAUGaxL.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-XC6FSj8.jpg](cities/city-XC6FSj8.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-eRBaK0b.jpg](cities/city-eRBaK0b.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-f93H1gd.jpg](cities/city-f93H1gd.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cities/city-xvKLvp0.jpg](cities/city-xvKLvp0.jpg) | [Romain Trystram][romaintrystram] | Unknown
| [cyberpunk/computer\_store-tumblr\_nm5cr8NyKo1re7gcxo2\_1280.gif](cyberpunk/computer_store-tumblr_nm5cr8NyKo1re7gcxo2_1280.gif) | - | Believed to be public domain*
| [cyberpunk/glowing.png](cyberpunk/glowing.png) | - | Believed to be public domain*
| [cyberpunk/office-94auWkd.jpg](cyberpunk/office-94auWkd.jpg) | - | Believed to be public domain*
| [outrun/city-2017.05.14.3.png](outrun/city-2017.05.14.3.png) | [Toyoi Yuuta][toyoiyuuta] | Unknown
| [outrun/landscape3-d7WVi4l.jpg](outrun/landscape3-d7WVi4l.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/landscape4-bah1gL9.jpg](outrun/landscape4-bah1gL9.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/landscape5-cRvOJM1.jpg](outrun/landscape5-cRvOJM1.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space-aAVsr1a.jpg](outrun/space-aAVsr1a.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space2-geVoeIR.jpg](outrun/space2-geVoeIR.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space3-SwSVePs.jpg](outrun/space3-SwSVePs.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space4-HUr8sp1.jpg](outrun/space4-HUr8sp1.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space5-HVaEDXh.jpg](outrun/space5-HVaEDXh.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/space6-Xdu6rbs.jpg](outrun/space6-Xdu6rbs.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/sun-RsQ32tC.jpg](outrun/sun-RsQ32tC.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/sun2-63jApDx.jpg](outrun/sun2-63jApDx.jpg) | [Nate Wren][natewren] | [Creative Commons Attribution-NonCommercial 4.0 International][cc-by-nc-4]
| [outrun/true_survivor-aYEG5sv.jpg](outrun/true_survivor-aYEG5sv.jpg) | [u/iancofino][iancofino] | Unknown

## Notes

- **\***: I really tried to find the original author, and the search eventually
  came up empty. If you know who made it, [let me
  know](https://codeberg.org/h3xx/dotfiles/issues).

<!-- License links -->
[cc-by-nc-4]: https://creativecommons.org/licenses/by-nc/4.0/

<!-- Citations -->
[iancofino]: https://www.reddit.com/r/outrun/comments/3r7e9m/the_results_of_listening_to_true_survivor_way_too/
[natewren]: https://natewren.com/rad-pack-80s-themed-hd-wallpapers/
[romaintrystram]: https://romaintrystram.myportfolio.com/
[toyoiyuuta]: https://1041uuu.tumblr.com/
