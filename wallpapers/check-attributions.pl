#!/usr/bin/perl
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;

use File::Find qw/ find /;
use FindBin qw//;

my $image_pat = qr/(?:png|xpm|gif|jpe?g|bmp|ico|tiff?|ppm|pnm|pbm|xbm|svg)$/i;
my $wallpapers_dir = $FindBin::RealBin;
my $readme_path = $FindBin::RealBin . '/README.md';

sub _files_in_readme {
    my @readmes = @_;
    my @filenames;
    my $parse_readme_line = sub {
        my $line = shift;
        chomp $line;
        if ($line =~ /^\s*\|\s*(?:\[(?<filename>[^\]]+)\])/) {
            (my $fn = $+{filename})
                =~ s/\\(.)/$1/g;
            push @filenames, $fn;
        }
        return;
    };
    foreach my $readme_filename (@readmes) {
        open my $fhi, '<', $readme_filename
            or croak("Failed to open file $readme_filename for reading: $!");
        while (my $line = <$fhi>) {
            $parse_readme_line->($line);
        }
        close $fhi;
    }
    return @filenames;
}

sub _images_in_collection {
    my @wallpaper_dirs = @_;
    my @images;
    foreach my $dir (@wallpaper_dirs) {
        my $base_path_escaped = quotemeta $dir;
        my $base_path_pat = qr{^$base_path_escaped/};
        find({
            no_chdir => 1,
            wanted => sub {
                my $fn = $File::Find::name;
                if (
                    -f $fn &&
                    $fn =~ $image_pat &&
                    $fn =~ $base_path_pat
                ) {
                    # Transform to path relative to $dir
                    $fn =~ s/$base_path_pat//;
                    push @images, $fn;
                }
            },
        }, $dir);
    }
    return @images;
}

my %has_attribution = (
    (map {
        ($_, 0)
    } _images_in_collection($wallpapers_dir)),
    (map {
        ($_, 1)
    } _files_in_readme($readme_path)),
);

my @attributed = sort grep {
    $has_attribution{$_}
} keys %has_attribution;

my @unattributed = sort grep {
    !$has_attribution{$_}
} keys %has_attribution;
if (@unattributed) {
    say 'The following files are missing attributions:';
    foreach my $filename (@unattributed) {
        say "- $filename";
    }
}

printf "%d/%d images properly attributed\n",
    scalar @attributed,
    scalar %has_attribution;

my @missing_files = sort grep {
    !-f "$wallpapers_dir/$_"
} keys %has_attribution;
if (@missing_files) {
    say 'The following files are referenced but are missing:';
    foreach my $filename (@missing_files) {
        say "- $filename";
    }
}

exit 2 if @missing_files;
exit 1 if @unattributed;
