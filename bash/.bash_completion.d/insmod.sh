# Module insertion commands see only modules
complete -o default -F _kmods insmod
_kmods() {
	readarray -t COMPREPLY < <(
		find "/lib/modules/$(uname -r)/" \
			-type f \
			\( -name "$2*.ko" -exec basename -- {} .ko \; \) -o \
			\( -name "$2*.ko.gz" -exec basename -- {} .ko.gz \; \) |
			sort -u
	)
}
