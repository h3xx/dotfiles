complete -o default -F _swaps swapoff
_swaps() {
	readarray -t COMPREPLY < <(
		/sbin/swapon -s |
		grep -o "^${2:-/}[^ ]*"
	)
}
