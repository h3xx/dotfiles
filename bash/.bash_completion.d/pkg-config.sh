# pkg-config
complete -o default -F _pkg_config pkg-config
_pkg_config() {
	local \
		PKG_FILES=() \
		PKG_PATH

	for PKG_PATH in ${PKG_CONFIG_PATH//:/ }; do
		PKG_FILES+=("$PKG_PATH/$2"*.pc)

		# No star globs
		[[ -f ${PKG_FILES[$((${#PKG_FILES[*]}-1))]} ]] ||
		unset PKG_FILES[$((${#PKG_FILES[*]}-1))]
	done

	# Basenames
	PKG_FILES=("${PKG_FILES[@]##*/}")

	COMPREPLY=(
		# Strip .pc
		"${PKG_FILES[@]%.pc}"
	)
}
