# psql sees users after -U
complete -o default -F _psqlcomp psql
_psqlcomp() {
	if [[ -f ~/.pgpass ]]; then
		readarray -t COMPREPLY < <(
			cut -f 4 -d ':' ~/.pgpass |
			grep "^$2"
		)
	fi
}
