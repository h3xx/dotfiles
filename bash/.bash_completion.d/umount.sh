# umount only sees mounted file systems
complete -o default -F _mounts umount
_mounts() {
	local MOUNTS MOUNTPATH
	readarray -t MOUNTS < /proc/mounts
	for MOUNTPATH in "${MOUNTS[@]#* }"; do
		MOUNTPATH=${MOUNTPATH%%' '*}
		if [[ $MOUNTPATH = "$2"* ]]; then
			COMPREPLY+=("$MOUNTPATH")
		fi
	done
}
