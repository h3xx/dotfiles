type _get_comp_words_by_ref &>/dev/null ||
_get_comp_words_by_ref() {
	while [[ $# -gt 0 ]]; do
		case $1 in
			cur)
				cur=${COMP_WORDS[$COMP_CWORD]}
				;;
			prev)
				prev=${COMP_WORDS[$COMP_CWORD-1]}
				;;
			words)
				words=("${COMP_WORDS[@]}")
				;;
			cword)
				cword=$COMP_CWORD
				;;
			-n)
				# Assume COMP_WORDBREAKS is already set sanely
				shift
				;;
		esac
		shift
	done
}
