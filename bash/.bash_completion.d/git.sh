# Turn on git programmable completion, if installed
test -n "$(
	for fn in \
		/usr/share/bash-completion/completions/git \
		/usr/doc/git-*.*.*/contrib/completion/git-completion.bash \
		; do
		if [[ -f $fn ]]; then
			echo "$fn"
			break
		fi
	done
)" && {
	. "$_" || return
	# Add alias completions that git-completion.bash isn't smart enough to
	# figure out
	_git_cherry_pick_wrapper() {
		_git_cherry_pick "$@"
	}
	_git_diff_wrapper() {
		_git_diff "$@"
	}
	_git_merge_into() {
		__git_complete_refs --dwim --mode=heads "$@"
	}
	_git_reset_wrapper() {
		_git_reset "$@"
	}
	# Alias for 'remote remove'
	_git_rr() {
		words[1]=remove
		_git_remote "$@"
	}
}
