# man completes on manual page names
# TODO: use $LANG settings?
complete -o default -F _manpages man
_manpages() {
	COMP_WORDBREAKS=${COMP_WORDBREAKS//:}
	[[ ${COMP_WORDS[1]} = */* ]] && return
	readarray -t COMPREPLY < <(
		COMP_WORDBREAKS=
		readarray -t MANPATHS < <(
			_MANPATH=$(manpath 2>/dev/null || command man -w 2>/dev/null)
			_MANPATH=${_MANPATH:-/usr/man}
			printf '%s\n' "${_MANPATH//:/$'\n'}"
		)
		if [[ $COMP_CWORD -gt 1 ]]; then
			SECT=${COMP_WORDS[1]}
		else
			SECT='[0-9]'
		fi
		shopt -s nullglob
		IFS=$'\n'
		for MP in "${MANPATHS[@]}"; do
			MANS=("$MP/man"${SECT}"/${2}"*.${SECT}*)
			if [[ ${#MANS[@]} -gt 0 ]]; then
				MANS=("${MANS[@]##*/}")
				echo "${MANS[*]%.[0-9]*}"
			fi
		done |sort -u
	)
}
