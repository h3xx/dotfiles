# Slackware packages
complete -o default -F _slackpacks removepkg
_slackpacks() {
	readarray -t COMPREPLY < <(
		shopt -s nullglob
		cd /var/log/packages
		m=("$2"*)
		if [[ ${#m[@]} -gt 0 ]]; then
			IFS=$'\n'
			echo "${m[*]%-+([^-])-+([^-])-+([^-])}"
		fi
	)
}
