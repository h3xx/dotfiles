# Other job commands
# (Addendum: job names not always good/legal)
#complete -j -P '%' fg jobs disown wait
#complete -A stopped -P '%' bg
complete -F _jobspecs -P '%' fg
_jobspecs() {
	readarray -t COMPREPLY < <(
		readarray JOBLINES < <(
			[[ $1 = 'bg' ]] && jobs -s || jobs
		)

		for JOBSPEC in "${JOBLINES[@]/]*}" ; do
			# truth
			# srch	| match	| ret
			# 0	| x	| 1
			# 1	| 0	| 0
			# 1	| 1	| 1
			# ret = !srch || match
			[[ -z ${2:1} || ${JOBSPEC:1} = "${2:1}"* ]] &&
			echo -n "${JOBSPEC:1} "
		done

		# Add job command names
		compgen -j -X '('
	)

	# This works but it uses too many external programs
	#read -a COMPREPLY <<EOF
#$(jobs $([ -z "${1/bg}" ] && echo '-s') |cut -c 2 |grep "^${2//%/}" |tr '\n' ' ')
#EOF
}
