complete -o default -F _artisan artisan
_artisan() {
	COMP_WORDBREAKS=${COMP_WORDBREAKS//:}
	local COMMANDS=$(
		while [[ $PWD != / ]]; do
			if [[ -e artisan ]]; then
				php artisan --raw --no-ansi list | cut -f 1 -d ' '
				break
			fi
			cd ..
		done
	)
	readarray -t COMPREPLY < <(
		compgen -W "$COMMANDS" -- "${COMP_WORDS[$COMP_CWORD]}"
	)
}
