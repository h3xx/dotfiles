complete -o default -F _gio gio
_gio() {
	if [[ $COMP_CWORD -eq 1 || ${COMP_WORDS[1]} = help ]]; then
		readarray -t COMPREPLY < <(
			compgen -W 'cat copy help info list mime mkdir monitor mount move open remove rename save set trash tree version' \
				-- "${COMP_WORDS[$COMP_CWORD]}"
		)
	fi
}
