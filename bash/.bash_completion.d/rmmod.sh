# Module removal commands only see installed modules
complete -o default -F _instkmods rmmod
_instkmods() {
	local MODULES NAME
	readarray -t MODULES < /proc/modules
	for NAME in "${MODULES[@]%% *}"; do
		if [[ $NAME = "$2"* ]]; then
			COMPREPLY+=("$NAME")
		fi
	done
}
