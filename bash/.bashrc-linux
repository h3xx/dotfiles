# .bashrc-linux
# universal defs

## bash builtin variables
HISTTIMEFORMAT='%s'		# the beloved Second of Our Linux
HISTFILESIZE=65535
HISTSIZE=65535
HISTIGNORE=${HISTIGNORE:+$HISTIGNORE:}'la:ll:lah:lat:;a:-:fg:bg:j:git +(s|si)*( ):rma:fol:.+(.)'
HISTCONTROL='ignoreboth'	# ignore duplicates and /^\s/
# Save history after every command
PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND;}'history -a'
[[ -e ~/.hosts ]] && HOSTFILE="$HOME/.hosts"	# for @-completion

# Small bash annoyances
shopt -s \
	cdspell \
	checkhash \
	checkwinsize \
	extglob \
	globstar \
	histappend \
	histreedit \
	histverify \
	no_empty_cmd_completion

shopt -u \
	sourcepath

# Use C-s to search forward through history (do not block output)
stty -ixon

# Edit this file
alias rc='$EDITOR ~/.bashrc-linux'

## make
# Utilize all cores
alias \
	make='make -j -'

## ls
alias \
	la='ls -la' \
	lah='la -h' \
	las='la -S' \
	lat='la -t' \
	ll='ls -l'

# Handy function to list all files and dirs in a single ls(1)
# Usefulness: ***
lsr() {
	find "${@:-.}" -print0 |
	# XXX Must sort or large file lists will be out of order if split up to
	# different ls invocations by xargs
	sort -z |
	xargs -0 \
		ls $LS_OPTIONS -dla
}

# Same thing as lsr but filters out .svn, .git, .hg, .bzr
# Usefulness: ****
lsrr() {
	gfind "${@:-.}" \
		-print0 |
	# XXX Must sort or large file lists will be out of order if split up to
	# different ls invocations by xargs
	sort -z |
	xargs -0 \
		ls $LS_OPTIONS -dla
}

# Same thing as lsr, but pipe it through less(1) with coloring
# Usefulness: **
lsrl() {
	find "${@:-.}" -print0 |
	# XXX Must sort or large file lists will be out of order if split up to
	# different ls invocations by xargs
	sort -z |
	xargs -0 \
		ls $LS_OPTIONS -dla --color=always |
	less -R
}

## grep
alias grep='grep --color=auto'

## find
# Usefulness: *****
alias \
	findi='find . -iname' \
	find1='find . -maxdepth 1 -mindepth 1' \
	find1i='find1 -iname' \
	findf='find . -type f' \
	findfi='findf -iname' \
	findf1='find1 -type f' \
	findf1i='findf1 -iname' \
	findd='find . -type d' \
	finddi='findd -iname' \
	findd1='find1 -type d' \
	findd1i='findd1 -iname'

# If piping output somewhere (like grep) write header to stderr and the rest of
# the lines to stdout.
# This makes the header show up in grepped results
# Usefulness: TODO
_header_shunt() {
	if [[ ! -t 1 ]]; then
		"$@" |sed -e '1w /dev/stderr' -e '1d'
	else
		"$@"
	fi
}

alias \
	lsof='_header_shunt lsof'

## ps

alias \
	ps='_header_shunt ps' \
	psa='ps -A x -f f' \
	psac='ps -A xwf -eo pid,user,cgroup,bsdtime,args' \
	psx='ps x f -o user,pid,ppid,c,stime,tty,stat,nice,bsdtime,cmd' \
	psg='ps -A x -f |grep'

## other programs
# xo = from work, helps greatly
alias \
	startx='exec startx' \
	g='gvim' \
	xo='xdg-open'

## directories
# NOTE: `autocd' shell option may be a better option than the ..* aliases
alias \
	..='cd .. && pwd && ls -A' \
	...='cd ../.. && pwd && ls -A' \
	....='cd ../../.. && pwd && ls -A' \
	.....='cd ../../../.. && pwd && ls -A' \
	......='cd ../../../../.. && pwd && ls -A' \
	.......='cd ../../../../../.. && pwd && ls -A' \
	........='cd ../../../../../../.. && pwd && ls -A' \
	-='cd - && ls -A' \
	++='pushd' \
	--='popd || popd -0'

## data sinks and dump files
n='/dev/null'
z='/dev/zero'
g="$HOME/.garbage.out"

## misc bash shortcuts
alias \
	k='kill -9' \
	ka='killall -v' \
	stopall='ka -s STOP' \
	startall='ka -s CONT' \
	stf='strace -etrace=file' \
	stfg='stf -o$g' \
	j='jobs' \
	rmr='rm -rf' \
	rmdirempty='findd -empty -print -delete' \
	rmdirempty1='findd1 -empty -print -delete'

# The "I forgot to use `CMD && ALSO`" aliases.
# E.g.:
# $ rsync ... <Enter>
# [as it's excuting]
# and rm -rf ...
# Usefulness: TODO
alias \
	and='[[ $? -eq 0 ]] &&' \
	or='[[ $? -eq 0 ]] ||'

# Create and switch to a new directory
# Usefulness: ****
nd() {
	[[ -n $1 && $1 != - ]] &&
	mkdir -pv -- "$1" &&
	cd "$1"
}

# "Follow" last argument
# Usefulness: ***
fol() {
	local \
		FLAG \
		LASTARG=$_ \
		OPTIND \
		CD_OP='cd'

	if [[ $LASTARG = - ]]; then
		LASTARG=$OLDPWD
	fi

	while getopts 'pr' FLAG; do
		case "$FLAG" in
			'p')
				CD_OP='pushd'
				;;
			'r')
				LASTARG=$(realpath -- "$LASTARG")
				;;
			*)
				printf 'Usage: fol [-p] [-r]\n' >&2
				return 2
		esac
	done

	if [[ ! -d $LASTARG ]] || ! "$CD_OP" "$LASTARG"; then
		"$CD_OP" "$(dirname -- "$LASTARG")"
	fi
}

# Clone and switch to a new directory
# Usefulness: ***
gcd() {
	git clone "$@" || return
	local TARGETDIR=$_ URL=$1
	if [[ $URL != "$TARGETDIR" && -d $TARGETDIR ]]; then
		cd "$TARGETDIR"
		return
	fi

	# Massage "/path/to/dir/.git/" --> "/path/to/dir"
	# Massage "/path/repo.git" --> "/path/repo"
	# XXX Requires "shopt -s extglob"!
	TARGETDIR=${TARGETDIR%%+(/|.git)}
	# Massage "prefix.dns:repo" --> "repo"
	TARGETDIR=${TARGETDIR#+([0-9A-Za-z.]):}
	# Massage "/path/to/repo" --> "repo"
	TARGETDIR=${TARGETDIR##*/}

	cd "$TARGETDIR"
}

# Quickly switch to a git root directory
# Usefulness: *****
gr() {
	local DIR
	DIR=$(git rev-parse --show-toplevel) &&
	cd "$DIR"
}

# Untar and cd to the tarball's directory
# Usefulness: *****
# Example:
# ~ $ tcd foo-1.2.3.tar.xz
# ~/foo-1.2.3 $ _
tcd() {
	local TARBALL
	if [[ $1 =~ (https?|ftp):// ]]; then
		TARBALL=${1##*/} # basename
		[[ -e $TARBALL ]] ||
		wget -O "$TARBALL" "$1" || {
			local ERR=$?
			rm -f -- "$TARBALL"
			return "$ERR"
		}
	else
		TARBALL=$1
	fi

	tar xf "$TARBALL" &&
	cd "$(tar tf "$TARBALL" |head -1 |cut -f 1 -d /)"
}

# Unzip and cd to the zip's directory
# Usefulness: *****
# Example:
# ~ $ zcd foo-1.2.3.zip
# ~/foo-1.2.3 $ _
zcd() {
	local ZIP
	if [[ $1 =~ (https?|ftp):// ]]; then
		ZIP=${1##*/} # basename
		[[ -e $ZIP ]] ||
		wget -O "$ZIP" "$1" || {
			local ERR=$?
			rm -f -- "$ZIP"
			return "$ERR"
		}
	else
		ZIP=$1
	fi

	unzipall "$ZIP" &&
	local BASE=${ZIP##*/} &&
	cd "${BASE%.*}"
}

# Turn off history (incognito mode)
# Usefulness: ?
histoff() {
	unset HISTFILE
	# De-colorize prompt, but set to bright white for clear indication that
	# history is off
	#PS1='\[\e[01;37m\]'"$(sed -e 's,\\\[\\e\[[^]]*\\\],,g' <<<"$PS1")"'\[\e[0m\]'
	# Same de-colorization, using __git_ps1
	PROMPT_COMMAND='printf '\''\e[01;37m'\'"; $(sed -e 's,\(\\\e\[[0-9;]*m\),,g;s,\\\[\\\],,g' <<<"$PROMPT_COMMAND")"
}
