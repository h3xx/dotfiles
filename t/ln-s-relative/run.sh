#!/bin/bash
# vi: et sts=4 sw=4 ts=4

WORKDIR=${0%/*}
. "$WORKDIR/../funcs.sh"
SCRIPT=$(realpath -- "$WORKDIR/../../home/bin/ln-s-relative")
TEMPDIR=$(mktemp -d -t "${0##*/}.XXXXXX")
cleanup() {
    rm -fr -- "$TEMPDIR"
}
trap 'cleanup' EXIT

test_non_traversing_link() {
    local -r WORKDIR=$TEMPDIR/t/$FUNCNAME
    mkdir -p "$WORKDIR" &&
    touch "$WORKDIR/file" &&
    "$SCRIPT" "$WORKDIR/file" "$WORKDIR/sym" || exit

    assert_symlink_is 'file' "$WORKDIR/sym"
}

test_multi_links() {
    local -r WORKDIR=$TEMPDIR/t/$FUNCNAME
    mkdir -p "$WORKDIR/dir1" "$WORKDIR/dir2" "$WORKDIR/subdir" &&
    touch "$WORKDIR/dir1/file1" &&
    touch "$WORKDIR/dir2/file2" &&
    touch "$WORKDIR/file" &&

    "$SCRIPT" \
        "$WORKDIR/dir2/file2" \
        "$WORKDIR/file" \
        "$WORKDIR/dir1/file1" \
        "$WORKDIR/subdir" || exit

    assert_symlink_is '../file' "$WORKDIR/subdir/file"
    assert_symlink_is '../dir1/file1' "$WORKDIR/subdir/file1"
    assert_symlink_is '../dir2/file2' "$WORKDIR/subdir/file2"
}

test_link_into_subdir() {
    local -r WORKDIR=$TEMPDIR/t/$FUNCNAME
    mkdir -p "$WORKDIR/subdir" &&
    touch "$WORKDIR/file" &&
    "$SCRIPT" "$WORKDIR/file" "$WORKDIR/subdir" || exit

    assert_symlink_is '../file' "$WORKDIR/subdir/file"
}

test_link_to_symlink() {
    local -r WORKDIR=$TEMPDIR/t/$FUNCNAME
    mkdir -p "$WORKDIR/subdir" &&
    touch "$WORKDIR/file" &&
    ln -s 'file' "$WORKDIR/alias" &&
    "$SCRIPT" "$WORKDIR/alias" "$WORKDIR/sym" || exit

    assert_symlink_is 'alias' "$WORKDIR/sym"

    # And inside a subdirectory
    "$SCRIPT" "$WORKDIR/alias" "$WORKDIR/subdir" || exit
    assert_symlink_is '../alias' "$WORKDIR/subdir/alias"
}

test_link_to_complex_symlink() {
    local -r WORKDIR=$TEMPDIR/t/$FUNCNAME
    mkdir -p "$WORKDIR/dir1" "$WORKDIR/dir2" "$WORKDIR/subdir" &&
    touch "$WORKDIR/file" &&
    ln -s '../file' "$WORKDIR/dir1/alias1" &&
    ln -s '../dir1/alias1' "$WORKDIR/dir2/alias2" &&
    "$SCRIPT" "$WORKDIR/dir2/alias2" "$WORKDIR/sym" || exit

    assert_symlink_is 'dir2/alias2' "$WORKDIR/sym"

    # And inside a subdirectory
    "$SCRIPT" "$WORKDIR/dir1/alias1" "$WORKDIR/dir2/alias2" "$WORKDIR/subdir" || exit
    assert_symlink_is '../dir2/alias2' "$WORKDIR/subdir/alias2"
    assert_symlink_is '../dir1/alias1' "$WORKDIR/subdir/alias1"
}

cd "$TEMPDIR" || exit
TEST_COUNT=0
TESTS_PASSED=0
for TESTNAME in \
    test_non_traversing_link \
    test_link_into_subdir \
    test_multi_links \
    test_link_to_symlink \
    test_link_to_complex_symlink \
    ; do

    (( ++TEST_COUNT ))
    if "$TESTNAME"; then
        echo_success "$TESTNAME"
        echo
        (( ++TESTS_PASSED ))
    else
        echo_failure "$TESTNAME failed"
        echo
    fi

done

cleanup

printf '%d/%d tests passed\n' "$TESTS_PASSED" "$TEST_COUNT"
if [[ $TESTS_PASSED -ne $TEST_COUNT ]]; then
    exit 1
fi
