au BufNewFile,BufRead */bin/*
	\,*/_configure/*
	\,*/build/scripts/*
	\ setf sh
