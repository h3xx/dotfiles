function! mylightline#Settings()
	" Note: This structure cannot contain funcrefs; all references to functions
	" must be global
	let settings={
		\ 'colorscheme': 'badwolf',
		\ 'component': {
		\   'lineinfo': '� %3l:%-2v',
		\ },
		\ 'active': {
		\   'left': [ [ 'mode', 'paste' ], [ 'fugitive' ], [ 'filename', 'file_flags' ] ],
		\ },
		\ 'component_function': {
		\   'file_flags': 'mylightline#FileFlags',
		\   'filename': 'mylightline#Filename',
		\   'fileformat': 'mylightline#Fileformat',
		\   'filetype': 'mylightline#Filetype',
		\   'fileencoding': 'mylightline#Fileencoding',
		\   'fugitive': 'mylightline#Fugitive',
		\ },
		\ 'separator': { 'left': '�', 'right': '�' },
		\ 'subseparator': { 'left': '>', 'right': '<' },
		\ 'tabline_separator': { 'left': '', 'right': '' },
		\ 'tabline_subseparator': { 'left': '', 'right': '' },
		\ }

	if &encoding == 'utf-8'
		let settings.separator.right='«'
		let settings.separator.left='»'
		let settings.component.lineinfo=substitute(settings['component']['lineinfo'], '�', '¶', 'g')
	endif

	return settings
endfunction

function! s:Readonly()
	return &ft != 'help' && &readonly ? '[RO]' : ''
endfunction

function! s:Modified()
	return &modified ? '[+]' : &modifiable ? '' : '[-]'
endfunction

function! mylightline#Filename()
	let fname=expand('%')
	return '' != fname ? fname : '[No Name]'
endfunction

function! mylightline#FileFlags()
	return s:Modified() .. s:Readonly()
endfunction

function! mylightline#Fileformat()
	" Only show unexpected fileformats
	return winwidth(0) > 70 && &fileformat != 'unix' ? &fileformat : ''
endfunction

function! mylightline#Filetype()
	return winwidth(0) > 70 ? &filetype : ''
endfunction

function! mylightline#Fileencoding()
	" Only show unexpected fileencodings (expected is &enc)
	return winwidth(0) > 70 && &fenc == &enc ? '' : &fenc ..
		\ (&bomb ? '[bom]' : '')
endfunction

if &encoding == 'utf-8'
	function! mylightline#Fugitive()
		if exists('*fugitive#head')
			let _=fugitive#head()
			return strlen(_) ? 'µ '.._ : ''
		endif
		return ''
	endfunction
else
	function! mylightline#Fugitive()
		if exists('*fugitive#head')
			let _=fugitive#head()
			return strlen(_) ? '� '.._ : ''
		endif
		return ''
	endfunction
endif
