" Note: The reason this is in after/ftplugin instead of regular ftplugin is
" that the system-wide ftplugin/diff.vim unconditionally sets b:undo_ftplugin.

" Undo mappings from our ftplugin/diff/keys.vim
" unmap = nunmap + vunmap + ounmap
" Also, 'exe' here because all :map commands can't |-break.
let b:undo_ftplugin ..=
	\ ' | sil! exe "unmap <buffer> <silent> [["' ..
	\ ' | sil! exe "unmap <buffer> <silent> ]]"' ..
	\ ' | sil! exe "unmap <buffer> <silent> [m"' ..
	\ ' | sil! exe "unmap <buffer> <silent> ]m"'
