" Note: The reason this is in after/ftplugin instead of regular ftplugin is
" that the system-wide ftplugin/php.vim unconditionally sets b:undo_ftplugin.

" Undo mappings from our ftplugin/php/keys.vim
" Also, 'exe' here because all :map commands can't |-break.
let b:undo_ftplugin = (exists('b:undo_ftplugin') ? b:undo_ftplugin .. '|' : '') ..
	\ ' sil! exe "nunmap <buffer> <silent> ,<C-d>"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,C"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,D"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,E"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,a"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,c"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,e"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,h"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,l"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,t"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> Q"'
" Undo mappings from our ftplugin/php/keys-phpunit.vim
let b:undo_ftplugin ..=
	\ ' | sil! exe "nunmap <buffer> <silent> ,d"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,K"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,M"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,P"'
