" Set up paths, which really helps when using 'gf'
" Note: The reason this is here instead of regular ftplugin/vim is that the
" system-wide ftplugin/vim.vim unconditionally sets b:undo_ftplugin.
setl path+=$VIMRUNTIME path+=$VIM/vimfiles

" Make sure switching file types undoes our change.
let b:undo_ftplugin = (exists('b:undo_ftplugin') ? b:undo_ftplugin .. '|' : '') ..
	\ 'setl path<'
