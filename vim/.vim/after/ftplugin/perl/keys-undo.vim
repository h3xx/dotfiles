" Note: The reason this is in after/ftplugin instead of regular ftplugin is
" that the system-wide ftplugin/perl.vim unconditionally sets b:undo_ftplugin.

" Undo mappings from our ftplugin/perl/keys.vim
" Also, 'exe' here because all :map commands can't |-break.
let b:undo_ftplugin = (exists('b:undo_ftplugin') ? b:undo_ftplugin .. '|' : '') ..
	\ ' sil! exe "nunmap <buffer> <silent> ,c"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,d"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,D"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,f"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,g"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,G"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,h"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,n"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,N"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,s"'
" Undo mappings from our ftplugin/perl/keys-jumptosub.vim
for s:mode in ['n', 'o', 'x']
	let b:undo_ftplugin ..=
		\ " | sil! exe '" .. s:mode .. "unmap <buffer> ]]'"
		\ " | sil! exe '" .. s:mode .. "unmap <buffer> [['"
endfor
unlet s:mode
