" Note: The reason this is in after/ftplugin instead of regular ftplugin is
" that the system-wide ftplugin/html.vim unconditionally sets b:undo_ftplugin.

" Undo mappings from our ftplugin/html/keys.vim
" Also, 'exe' here because all :map commands can't |-break.
let b:undo_ftplugin ..=
	\ ' | sil! exe "nunmap <buffer> <silent> ,h"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,j"' ..
	\ ' | sil! exe "nunmap <buffer> <silent> ,l"'
