" The system-wide after/syntax/lisp.vim sets system encoding to utf-8 for some
" reason. It's done that for as long as I can remember.
" Syntax files should not alter system encoding.
" Affects: lisp, rst, rrst

set enc&
