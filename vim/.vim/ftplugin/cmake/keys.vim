vim9s

def Jump(pattern: string, count: number, flags: string)
	normal! m'
	for i in range(count)
		if !search(pattern, flags)
			break
		endif
	endfor
enddef

# Use [[, ]] to jump between functions
final fpat = '^\s*function'
for mode in ['n', 'o', 'x']
	exe mode .. "no <buffer> <silent> ]] <Cmd>call <SID>Jump('" .. fpat .. "', v:count1, 'W')<CR>"
	exe mode .. "no <buffer> <silent> [[ <Cmd>call <SID>Jump('" .. fpat .. "', v:count1, 'bW')<CR>"
endfor
