nnoremap <buffer> ,R iReference ticket: 
nnoremap <buffer> ,U :normal iUpdate submodule pointers<cr>
" Re-commit (roughly the same as 'git rc')
nnoremap <buffer> <silent> rc :Gcommit -c HEAD --date=now<cr>
