" Fold the header
setl foldmethod=expr foldexpr=getline(v:lnum)=~'^\"' foldlevel=0

" (Syntax method, doesn't quite work because I'm a n00b)
"syn region foldNetrwComment start=/"\ =\+/ end=/"\ =\+/ transparent fold keepend

" Bug workaround:
" Set bufhidden=wipe in netrw windows to circumvent a bug where Vim won't let
" you quit (!!!) if a netrw buffer doesn't like it.
" Also buftype should prevent you from :w
" (Reproduce this bug by opening netrw, :e ., :q)
setl bufhidden=wipe buftype=nofile
