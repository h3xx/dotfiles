" Fix some key maps set by autoload/netrw.vim

" Mapped to NetrwRefresh by default; interferes with global map in with .vimrc
silent! nunmap <buffer> <silent> <C-l>

" brainspace--: Make the key map to open a file in a new tab the same as
" opening a new tab from a Ctrl-P listing.
nmap <buffer> <C-g> t
