" Don't insert crazy indentation when indenting '\' lines.
" (Default is shiftwidth() * 3. One is enough.)
" Used by $VIMRUNTIME/autoload/dist/vimindent.vim
let g:vim_indent={
	\ 'line_continuation': 'shiftwidth()',
	\ }
