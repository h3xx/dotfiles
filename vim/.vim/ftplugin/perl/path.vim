" Needed for ftplugin/perl.vim to populate g:perlpath, l:path
let g:perl_exec=1

" Note: the system-wide ftplugin/perl.vim already sets up b:undo_ftplugin to
" undo 'setl path' changes.
setl path+=lib

" Add <GIT ROOT>/lib to path
if exists('*fugitive#head')
	silent! let &l:path = &l:path .. ',' .. fugitive#repo().tree() .. '/lib'
endif
