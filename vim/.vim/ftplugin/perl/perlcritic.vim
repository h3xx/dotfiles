if executable('perlcritic')
	" Syntax check the current file by running :make.
	" Note: Include a .perlcriticrc in the current directory (:pwd) or your
	" home directory to configure.
	" --quiet: Suppress the "source OK" message when no violations are found.
	" --verbose NUM|FORMAT: Set a machine-readable output format.
	setl makeprg=perlcritic\ --quiet\ --nocolor\ --verbose\ 1\ %
	setl errorformat=%f:%l:%c:%m
endif
