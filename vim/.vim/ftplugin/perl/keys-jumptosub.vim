" Use [[, ]] to jump between subs
fu! s:jumpToSub(forward)
	call search('\(^\|\<\)', a:forward . 'sW', 0, 0, "synIDattr(synID(line('.'), col('.'), 1), 'name') !~# '\\<\\(perlFunction\\|perlStatementPackage\\)\\>'")
endfu
for s:mode in ['n', 'o', 'x']
	exe s:mode .. "noremap <silent> <buffer> ]] <Cmd>call <SID>jumpToSub('')<CR>"
	exe s:mode .. "noremap <silent> <buffer> [[ <Cmd>call <SID>jumpToSub('b')<CR>"
endfor
unlet s:mode
