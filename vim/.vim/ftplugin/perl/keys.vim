" Header command
if expand('%:e') == 'pm'
	" ,c - insert guessed package name, useful for package files
	nnoremap <buffer> <silent> ,c :ru templates/keys/perl-replace-word-with-package-name.vim<CR>

	" ,h - insert header text
	nnoremap <buffer> <silent> ,h :ru templates/headers/perl-pm.vim<CR>
elseif expand('%:e') == 'cgi'
	nnoremap <buffer> <silent> ,h :ru templates/headers/perl-cgi.vim<CR>
elseif expand('%:e') == 't'
	nnoremap <buffer> <silent> ,h :ru templates/headers/perl-t.vim<CR>
else
	nnoremap <buffer> <silent> ,h :ru templates/headers/perl.vim<CR>
endif

" Common key combinations

" ,c - insert Carp::croak import
nnoremap <buffer> <silent> ,c :ru templates/keys/perl-carp-croak.vim<CR>

" ,d - insert Data::Dumper call
nnoremap <buffer> <silent> ,d :ru templates/keys/perl-data-dumper.vim<CR>

" ,D - insert sub deprecation warning
nnoremap <buffer> <silent> ,D :ru templates/keys/perl-deprecation-sub.vim<CR>

" ,f - insert module load path from FindBin
nnoremap <buffer> <silent> ,f :ru templates/keys/perl-findbin.vim<CR>

" ,g - insert Getopt::Std option processing
nnoremap <buffer> <silent> ,g :ru templates/keys/perl-getopts.vim<CR>

" ,G - insert Getopt::Long option processing
nnoremap <buffer> <silent> ,G :ru templates/keys/perl-getopt-long.vim<CR>

" ,n - insert 'sub new' declaration for packages
nnoremap <buffer> <silent> ,n :ru templates/keys/perl-sub-new.vim<CR>

" ,N - insert 'sub new' declaration for fields-ware packages
nnoremap <buffer> <silent> ,N :ru templates/keys/perl-sub-new-fields.vim<CR>

" ,o - insert file opener
nnoremap <buffer> <silent> ,o :ru templates/keys/perl-open-file.vim<CR>

" ,s - insert file handle slurp
nnoremap <buffer> <silent> ,s :ru templates/keys/perl-slurp.vim<CR>

" ,t - insert temporary file
nnoremap <buffer> <silent> ,t :ru templates/keys/perl-mktemp.vim<CR>

" ,T - insert temporary directory
nnoremap <buffer> <silent> ,T :ru templates/keys/perl-temp-dir.vim<CR>
