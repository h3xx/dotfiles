if expand('%:e') == 'rb'
	" ,c - insert guessed package name, useful for class files
	nnoremap <buffer> <silent> ,c :ru templates/keys/ruby-replace-word-with-package-name.vim<CR>
endif
