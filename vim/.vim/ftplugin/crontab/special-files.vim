if @% =~# '^/tmp/crontab\.'
	" Don't keep metadata around for ephemeral 'crontab -e' files
	setl nobk noswf noudf
endif
