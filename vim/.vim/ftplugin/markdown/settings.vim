" Markdown looks better with these space settings
setl et sts=2 sw=2 ts=4
" Turn on spell check
setl spell
