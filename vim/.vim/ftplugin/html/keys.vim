" Common key combinations

" ,h - insert HTML header in HTML files
nnoremap <buffer> <silent> ,h :ru templates/keys/html5-file-header.vim<CR>
" ,j - include jquery
nnoremap <buffer> <silent> ,j :ru templates/keys/html5-jquery.vim<CR>
" ,l - <link rel="stylesheet">
nnoremap <buffer> <silent> ,l :ru templates/keys/html5-link-rel-stylesheet.vim<CR>
