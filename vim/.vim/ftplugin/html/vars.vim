" Indent for attributes after an open <tag line:
" 1  auto indent, one indent step more than <tag
" 2  auto indent, two indent steps (default)
" >2 auto indent, more indent steps
let b:html_indent_attribute=1

" Fix excessive indent on multi-line attributes.
" E.g.:
" <div style="
"             foo: bar
"             ">
"
" Indents to instead:
" <div style="
"     foo: bar
" ">
fu! s:html_indent_tag_string_func(lnum)
	" This function is only called if the start of the line being indented lies
	" within an attribute string.
	" Note a:lnum is the PREVIOUS line number.
	if stridx(getline(a:lnum + 1), '"') != -1
		" End of string, e.g. '">'
		return indent(a:lnum) - shiftwidth()
	en
	return indent(a:lnum) + shiftwidth()
endfu
let b:html_indent_tag_string_func=function('s:html_indent_tag_string_func')
