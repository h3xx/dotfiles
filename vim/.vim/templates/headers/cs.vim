insert
/*
Copyright (C) YEAR Dan Church.
.
s/YEAR/\=strftime("%Y")/
append
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
*/
namespace foo {
    using System;

    class bar {
	static int Main() {

// vi: sw=4 sts=4
.
