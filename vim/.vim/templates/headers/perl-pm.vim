" header for perl modules

insert
package PACKAGE 0.000001;
.
norm W
ru templates/keys/perl-replace-word-with-package-name.vim

append
# vi: et sts=4 sw=4 ts=4
use 5.012;
use warnings;

.

ru templates/keys/perl-sub-new.vim

append
=head1 AUTHOR

Dan Church S<E<lt>h3xx@gmx.comE<gt>>

=head1 LICENSE AND COPYRIGHT

Copyright (C) YEAR Dan Church.
.
s/YEAR/\=strftime("%Y")/
append

License GPLv3: GNU GPL version 3.0 (L<https://www.gnu.org/licenses/gpl-3.0.html>)
with Commons Clause 1.0 (L<https://commonsclause.com/>).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.

=cut

1;
.

" Skip back up to package body
norm 5G
