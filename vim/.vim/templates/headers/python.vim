" header for Python scripts

if executable('python3')
    insert
#!/usr/bin/python3
.
else
    insert
#!/usr/bin/python
.
endif

append
# vi: et sts=4 sw=4 ts=4
.
