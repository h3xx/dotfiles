" Insert a sub deprecation warning
" Depends on "use warnings" which should you be using everywhere anyway.
" Turn off this warning with "no warnings 'deprecated'" in calling code.

append
    warnings::warnif('deprecated', (caller 0)[3] . ' is deprecated');
.
