" New package instantiation method for Perl

append
sub new {
    my ($class, %args) = @_;
    return bless {
        %args,
    }, $class;
}

.
