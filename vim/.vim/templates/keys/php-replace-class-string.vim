" Replace class name with ::class shortcut.
" Not really necessary, but it helps IDE's with determining if a class you're
" referencing really exists.
" Depends on vim-surround ("ysi,]")

" Replaces:
" '\Fully\Qualified\Class\Name' -> \Fully\Qualified\Class\Name::class
" 'Fully\Qualified\Class\Name' -> \Fully\Qualified\Class\Name::class

" Remove the quotes (ds')
" Jump to the character where the last quote was ('')
" Add "::class"
norm ds'''a::class
" Fixup relative class name (Foo\Bar -> \Foo\Bar)
norm ''
if getline('.')[col('.')-1] != '\'
	norm i\
endif
