" Quickie key sequence to clean up and auto-format imports

" Before:
" import {
" Foo as   Quux        , Baz, Qux } from '@foo';
"
" After:
" import {
"     Foo as Quux,
"     Baz,
"     Qux,
" } from '@foo';

" Join the import
normal vaBJ

" Watch out for "Foo as Bar" imports
silent! s/  *as  */__AS__/g

silent! s/,//g

" Surround with newlines
s/{/{\r/
s/}/\r}/

normal k<<

" Split lines
silent! s/  */,\r/g
normal dd

" Indent everything properly
normal =iB

" Watch out for "Foo as Bar" imports
silent! %s/__AS__/ as /g

noh
