" Getopt::Long call for Perl

append
use Getopt::Long qw/ GetOptions :config bundling no_getopt_compat no_ignore_case /;

my (
    $opt_foo,
    $help,
);
unless (GetOptions(
    'foo' => \$opt_foo,
    'no-foo' => sub { $opt_foo = 0 },
    'help' => \$help,
)) {
    exit 2;

    #use Pod::Usage qw/ pod2usage /;
    #pod2usage(
    #   -exitval => 2,
    #   -msg => q{Try 'SCRIPTNAME --help' for more information},
.
s/SCRIPTNAME/\=expand('%:t')/
append
    #);

}

#if ($help) {
#    pod2usage(
#        -verbose => 1,
#        -exitval => 0,
#    );
#}
.
