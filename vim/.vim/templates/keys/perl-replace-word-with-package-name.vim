" Replace current WORD with the current guessed package name based on the file
" path
let s:parts=[expand('%:t:r')]
let s:dirpath=expand('%:p:h')
while s:dirpath =~ '/[A-Z][^/]*$'
	call add(s:parts, fnamemodify(s:dirpath, ':t'))
	" Go up one level
	let s:dirpath=fnamemodify(s:dirpath, ':h')
endwhile
exe "norm \"_ciW\<C-r>=join(reverse(s:parts), '::')\<CR>"

unlet s:dirpath s:parts
