insert
(build_script=$(readlink -f -- "$BASH_SOURCE")
conf_script=${build_script%-build}
[[ $build_script = "$conf_script" || ! -f $conf_script ]] ||
. "$conf_script") &&
DESTDIR=$PWD/b ninja -C _build install &&
mkdir -p \
    b/install \
    "b/$(svn-docdir)" &&
.
