" New fields-aware package instantiation method for Perl

append
use fields qw/
    field1
/;

sub new {
    my ($class, %args) = @_;
    my PACKAGE $self = $class;
.
norm w
ru templates/keys/perl-replace-word-with-package-name.vim

append
    unless (ref $self) {
        $self = fields::new($class);
    }
    # Init base fields
    #$self->SUPER::new();
    %{$self} = (%{$self}, %args);
    return $self;
}

.
