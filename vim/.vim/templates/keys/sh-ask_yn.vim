" handy yes/no prompt function

append
ask_yn() {
    local -r PROMPT=$1 DEFAULT=$2
    local ANSWER
    read -r -p "$PROMPT [$([[ $DEFAULT = 'y' ]] && echo 'Y/n' || echo 'y/N')]? " ANSWER
    [[ $ANSWER =~ [01NYny] ]] || ANSWER=$DEFAULT
    [[ $ANSWER =~ [1Yy] ]]
}
.
