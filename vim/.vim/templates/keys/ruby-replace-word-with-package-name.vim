" Replace current WORD with the current guessed package name based on the file
" path
exe "norm \"_ciW\<C-r>=expand('%:t:r')->substitute('\\v%(^|_)(.)', '\\u\\1', 'g')\<CR>"
