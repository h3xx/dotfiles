" ***********************
" ***** Environment *****
" ***********************

" Use Vim settings, rather then Vi settings (much better!). (default)
" This must be first, because it changes other options as a side effect.
"set nocompatible

" Use filetype and indent plugins (default)
"filetype plugin indent on

" ****************************************
" ***** Environment, Unicode options *****
" ****************************************

if has('multi_byte')
	" Set the display encoding
	" (default is '', or 'utf-8' in the GUI)
	" Addendum: IDK, this seems to work for everything, however something
	" happens to set &termencoding to always be 'utf-8' at this point.
	" Settings at user interaction time:
	" term	LC_ALL	| &encoding	&termencoding
	" --------------+----------------------------
	" GUI	*	| utf-8		utf-8
	" xterm	*.utf8	| utf-8		utf-8
	" xterm -	| utf-8		latin1
	" uxterm -	| utf-8		utf-8
	if !has('gui_running')
		let &termencoding=&encoding
	else
		set encoding=utf-8
	endif
	" Set the internal encoding
	" Addendum: messes up terminal encoding detection
	"set encoding=utf-8

	" &fileencoding (controls how characters in the internal encoding will be
	" written to the file) will be set according to &fileencodings
	" (default: 'ucs-bom', 'ucs-bom,utf-8,default,latin1' when &encoding is set
	" to a Unicode value)
endif " has('multi_byte')

" *****************
" ***** Mouse *****
" *****************

if has('mouse')
	"" When and where to use the mouse
	" n Normal mode
	" v Visual mode
	" i Insert mode
	" c Command-line mode
	" h all previous modes when editing a help file
	" a all previous modes
	" r for hit-enter and more-prompt prompt
	" use mouse all the time (default)
	"set mouse=a
	" Never use the mouse!
	" (middle-click for pasting in a terminal will still work)
	set mouse=

	" Whether the window focus follows the mouse (default off)
	"" (I can see this becoming very annoying)
	"set nomousefocus
endif " has('mouse')

" ************************
" ***** Backup files *****
" ************************

" Always keep a backup (default)
"set backup

" ********************************
" ***** Swap files, metadata *****
" ********************************

" Hide those backups
set backupdir=~/.vim/temp//

" Keep vim swap files out of the current working directory
set directory=~/.vim/temp//

" Keep vim undo files out of the current working directory
set undodir=~/.vim/temp//

" Number of characters typed before swap file is written (default)
"set updatecount=200

" Number of milliseconds in interval between swap file writes (default)
"set updatetime=4000

" Keep 512 lines of command line history
set history=512

" Number of undo levels (default)
"set undolevels=1000

" Supplemental spell file
"set spellfile=~/.vim/spell/en.ascii.add
"let &spellfile='~/.vim/spell/'.&spelllang.'.'.&encoding.'.add'

" *********************************
" ***** Filesystem navigation *****
" *********************************

" Change directory to match current buffer
"set autochdir
" (Slightly better: Don't chdir when buffer is on, e.g. a fugitive commit
" buffer, or a :Gblame buffer)
aug AutoChdir
	au!
	au BufEnter,BufFilePost * if expand('%:p') !~ '\(/\.git/\|\.fugitiveblame$\)' | silent! lcd %:p:h | endif
aug END

" Automatically read a file if changed outside of vim (thanks vim-sensible)
set autoread

" Ensure when opening in tabs, no tiny split windows (thanks vim-sensible)
set tabpagemax=50

" ************************
" ***** Command mode *****
" ************************

" Display incomplete commands (default)
"set showcmd

" Nice tab-completion on the command line
set wildmenu
" Disable VCS files
set wildignore+=.git,.svn,CVS
" Disable output files
set wildignore+=*.o,*.obj,*.class,*.gem
" Disable temp and backup files
set wildignore+=*.swp,*~
" git merge poop files
set wildignore+=*_BACKUP_*,*_BASE_*,*_LOCAL_,*_REMOTE_*,*.orig

" ******************************
" ***** Command mode, grep *****
" ******************************

" To get ack, use:
" $ wget -O ~/bin/ack http://beyondgrep.com/ack-2.14-single-file && chmod +x ~/bin/ack
if executable('ack')
	" Use ack over grep (automatically ignores *~)
	set grepprg=ack\ --nogroup\ --nocolor
else
	" Ignore backup files when using :grep
	set grepprg=grep\ -n\ --exclude=*~\ $*\ /dev/null
endif " executable('ack')

" ***********************************
" ***** Normal mode, navigation *****
" ***********************************

" Do incremental searching (default)
"set incsearch

" Set lines to scroll when the cursor moves off screen (default)
"set scrolljump=1

" Start scrolling when the cursor is x lines away from bottom/top margins
" (default)
"set scrolloff=5

" Do not time out when entering mapped key sequences
" Addendum: Does not work well with imapped F1-F10 keys because they send "\eO?
" in xterm
"set notimeout

" ********************************
" ***** Normal mode, editing *****
" ********************************

" Default ai- and gq-wrapping width
" (prevents vim from defaulting to using the terminal width)
set textwidth=79

" Don't add two spaces after a sentence-ending mark when gq-ing and j-ing
set nojoinspaces

" Don't consider "octal" numbers when using C-a and C-x (default)
"set nrformats-=octal

" ***********************
" ***** Insert mode *****
" ***********************

" Make indenting less stupid
set autoindent smartindent

" Less conspicuous tab indentation
set tabstop=4 shiftwidth=4

"" Bad options
" a	Automatic formatting of paragraphs. Every time text is inserted or deleted
"	the paragraph will be reformatted. See auto-format. When the 'c' flag is
"	present this only happens for recognized comments.
" n	When formatting text, recognize numbered lists. This actually uses the
"	'formatlistpat' option, thus any kind of list can be used. The indent of the
"	text after the number is used for the next line. The default is to find a
"	number, optionally followed by '.', ':', ')', ']' or '}'. Note that
"	'autoindent' must be set too. Doesn't work well together with "2".
"	Example: >
"		1. the first item
"		   wraps
"		2. the second item
" o	Automatically insert the current comment leader after hitting 'o' or
"	'O' in Normal mode.
" t	Automatic formatting of text using textwidth (but not comments)
" w	Trailing white space indicates a paragraph continues in the next line.
"	A line that ends in a non-white character ends a paragraph.
set fo-=a
\ fo-=n
\ fo-=o
\ fo-=t
\ fo-=w

"" Good options
" c	Auto-wrap comments using textwidth, inserting the current comment leader
"	automatically.
" r	Automatically insert the current comment leader after hitting <Enter>
"	in Insert mode.
" q	Allow formatting of comments with "gq" (here, mapped to "Q"). Note that
"	formatting will not change blank lines or lines containing only the comment
"	leader. A new paragraph starts after such a line, or when the comment leader
"	changes.
set formatoptions+=crq

" Taken from vim-sensible (https://github.com/tpope/vim-sensible)
if v:version > 703 || has('patch541')
	" Delete comment character when joining commented lines
	set formatoptions+=j
endif

" *******************
" ***** Folding *****
" *******************

" (addendum: as I become more experienced, folding means less to me than quick
" navigation, i.e. if you depend on folds, you're doing it wrong)
" 0: all folds closed upon opening a file
" 99: all folds open (close with `zc')
set foldlevelstart=99

" ************************
" ***** Key bindings *****
" ************************

" Don't use Ex mode; use Q for formatting (default)
"map Q gq

" Allow backspacing over everything in insert mode (default)
"set backspace=2

" Two-stroke saving instead of four-stroke
" Usefulness: *****
nnoremap <C-s> :w<cr>

" tmux integration
if exists('$TMUX')
	" Re-run the last command in the last-selected pane
	nnoremap <silent> \r :call system("tmux send-keys -t ':.!' C-p C-j")<CR>
endif

" Spacebar = page down
" (Ala less(1), web browsers, others)
" Usefulness: ***
nnoremap <Space> <C-d>

" Alt+s, Alt+p, Alt+f = common fugitive commands
" Rhymes with .inputrc mapping
" Usefulness: *****
nnoremap <A-s> :Gst<cr>
" Usefulness: ****
nnoremap <A-p> :Gpush 
" Usefulness: **
nnoremap <A-f> :Gfetch
" Usefulness: ***
nnoremap <A-u> :Gpull
" Usefulness: TODO
nnoremap <A-o> :Git co 

" git blame
" Usefulness: ****
nnoremap <F2> :Gblame<cr>

" Backslash-pipe to quickly comment lines using vim-commentary
" (also use "gcc" in normal, or "gc" in visual mode)
nmap \| <Plug>CommentaryLine
vmap \| <Plug>Commentary

" Reverse function of ' and `
" ' => more accurate jumping, ` => less accurate jumping
" Usefulness: ****
noremap ` '
noremap ' `

" Search for v-highlighted string
" Usefulness: TODO
vnoremap // y/\V<C-R>"<CR>

" Remove search highlighting
" Usefulness: ****
nnoremap <F1> :noh<CR>

" Force opening new tabs when gf-ing
" Usefulness: ***
nnoremap gf <C-W>gf

" Remap ctrl-[direction] to window moving
" (thanks to github.com/bling/minivimrc)
" Usefulness: *****
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" \v => re-select the text you just pasted
" Usefulness: **
nnoremap <leader>v V`]

" Yank from the cursor to the end of the line, to be consistent
" with C and D.
" Usefulness: **
nnoremap Y y$

" H/L navigation = beginning/end of line
" (I have never pressed H or L expecting what they do by default)
" Usefulness: *****
noremap H ^
noremap L $

" Override default C-u, C-d scrolling because I don't like it
" Usefulness: ***
nnoremap <C-u> 10k
nnoremap <C-d> 10j

" Sort indented lists
" Usefulness: ****
nmap \s vii:sort u<cr>
" Same thing but case-insensitive and only considering letters
" Note: Do NOT replace with 'sort -u', it's not the same as piping to uniq(1).
" 'sort -u' will remove entries that match case-insensitive counterpart.
nmap \S vii:!sort -fd\|uniq<cr>

" ,v - insertion of vim hintline
nnoremap <silent> ,v :ru templates/keys/hintline.vim<cr>

" ************************************
" ***** Key bindings, paste mode *****
" ************************************

" Paste mode
" Usefulness: ****
map <F10> :set paste<CR>
map <F11> :set nopaste<CR>
imap <F10> <C-O>:set paste<CR>
imap <F11> <nop>
set pastetoggle=<F11>

" *******************
" ***** Display *****
" *******************

" Disable preview window when completing
set completeopt-=preview
" Make it obvious I'm completing
set completeopt+=menuone

" Do not redraw screen until macros, etc. are done drawing to the screen
" (better responsiveness over SSH and slow terminals)
set lazyredraw

" Show line numbers
set number

" Show the cursor position all the time (default)
"set ruler

" I: Disable startup message
" c: Don't give useless ins-completion-menu messages in the statusbar
set shortmess+=Ic

" Show the current mode (default)
"set showmode

" Don't set the terminal title
" Addendum: in a screen(1) session this means setting the text on the status
" bar, which is actually nice
" addendum1: tmux not so much
set notitle

" Send more drawing commands to the terminal
set ttyfast

" ***************************
" ***** Display, colors *****
" ***************************

" Switch syntax highlighting on when the terminal has colors (default)
"syntax on
" Also switch on highlighting the last used search pattern. (default)
"set hlsearch

" Force more colors in the terminal than vim thinks is possible
" (Addendum: fixed 'the Right Way' by setting TERM=xterm-256color)
"set t_Co=16 t_AB=[48;5;%dm t_AF=[38;5;%dm

" Replace blinding gvim color scheme (makes terminal vim brighter)
set bg=dark
colorscheme hybrid

" Correct some colors
" (addendum: only affects terminal vim, looks better regular)
"highlight PreProc ctermfg=Magenta

" Highlight the current cursor line
"set cursorline
" Addendum: hide the cursorline on inactive windows
if ! &diff
	aug CursorLine
		au!
		au BufWinEnter,VimEnter,WinEnter * setl cursorline
		au WinLeave * setl nocursorline
	aug END
endif

" ********************************
" ***** Fancy plugin options *****
" ********************************

" *** lightline.vim ***

set laststatus=2 noru noshowmode

let g:lightline=mylightline#Settings()

" *** vim-gitgutter ***
" Wait until I switch buffers to update signs
let g:gitgutter_realtime=0
" Support non-utf8 terminals
let g:gitgutter_sign_removed_first_line='^'

" *** tagbar ***
if has('gui_running')
	nmap <silent> <F8> :packadd tagbar\|if &co<113\|set co=113\|endif\|TagbarToggle<CR>
else
	nmap <silent> <F8> :packadd tagbar\|TagbarToggle<CR>
endif
let g:tagbar_autofocus=1
let g:tagbar_ctags_bin='ctags'
"let g:tagbar_expand=1 " doesn't work too well

" *** ctrl-p ***
" c-t is unusable here because it's mapped in tmux
let g:ctrlp_prompt_mappings={
	\ 'AcceptSelection("t")': ['<c-g>'],
	\ }

" Don't get lost in the largest man-made structure on earth
let g:ctrlp_custom_ignore={
	\ 'dir': '\v[\/](node_modules|\.vim/temp)$',
	\ }
let g:ctrlp_show_hidden=1

" If multiple vim sessions are open in the same directory, it'll clobber each
" others' caches.
" Work around stale caches by pressing F5 in the Ctrl P search panel.
let g:ctrlp_clear_cache_on_exit=0

" Store up to 100 results in the window, let me scroll (C-j/k)
let g:ctrlp_match_window='results:100'

" Don't suddenly stop indexing after 10k files, keep going!
let g:ctrlp_max_files=0

" *** editorconfig-vim ***
let g:EditorConfig_disable_rules=[
	\ 'charset',
	\ 'trim_trailing_whitespace',
	\ ]
let g:EditorConfig_exclude_patterns=['fugitive://.*', 'scp://.*']
let g:EditorConfig_preserve_formatoptions=1

" *** arcane plugins ***
" Prevent some built-in, unused plugins from loading
let g:loaded_vimballPlugin=1
let g:loaded_getscriptPlugin=1

" ***********************************
" ***** Syntax specific options *****
" ***********************************
" (note: /ftplugin/* executes after these options are interpreted, so they must
" be defined here)

"" Perl extra coloring options
let perl_extended_vars=1
let perl_want_scope_in_variables=1
let perl_include_pod=1

"" PHP-Indenting-for-VIm
" No stupid arrow indenting esp. during mock object creation
let g:PHP_noArrowMatching=1
" Indent case statements according to PEAR standards
let g:PHP_vintage_case_default_indent=1

"" sh
" g:is_sh Borne shell (default)
" g:is_kornshell ksh
" g:is_posix same as ksh
" g:is_bash bash
let g:is_bash=1

"" netrw
" Press gx in normal mode to open the URL under the cursor
let g:netrw_browsex_viewer='google-chrome'
" Use tree style with decorations
let g:netrw_liststyle=3
" Show the banner but fold it (see .vim/after/ftplugin/netrw.vim)
let g:netrw_banner=1
" Horizontally split the window when opening a file via <cr>
let g:netrw_browse_split=4
" Preview window shown in a vertically split window
let g:netrw_preview=1
" Split files below, right
let g:netrw_alto=1
let g:netrw_altv=1
" Ignore some filename patterns
" Note: netrw loves to append characters to filenames sometimes for no reason
" All entries:    [/*|@=]\?
" Regular files:  [*|@=]\?
" Directories:    /
" Ignore dot files, swap files and backup files (regular files only)
let g:netrw_list_hide='\(\~\|^\..*\.swp\)[*|@=]\?$'
" Ignore all RCS dirs (directories only)
let g:netrw_list_hide.=',^\(CVS\|\.git\|\.svn\|\.hg\)/$'
" Ignore CVS backups (regular files only)
let g:netrw_list_hide.=',^\.#.*[*|@=]\?$'

let g:netrw_hide=1
" Note: Setting this variable causes weird effects when opening files via <cr>
"let g:netrw_winsize=26
" Activate netrw at 26 characters width when pressing the minus key
nmap <silent> - :26Vexplore<CR>

" Ensure every file opened from the command line gets opened in its own tab
" (except when running vimdiff)
" The same effect can be accomplished by running 'vim -p FILES'
" (note: this must occur last because sometime netrw causes issues)
if ! &diff
	tab all
endif
