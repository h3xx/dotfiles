# Frequently Asked Questions Regarding the License Change

## When did the license change?

It changed on 2022-07-20.

## How did the license change?

The license changed from a permissive MIT license to GPLv3 with Commons
Clause addendum.

## tl;dr: What does this mean?

The license change means new licensees are no longer able to use the software
or derivative works for commercial purposes.

## Why did you change the license?

This was supposed to be a fun project, but the thought of it being used to line
the pockets of billionaires takes all the fun out of it for me. They can make
their own software.

## I copied and made a derivative work from this software on [date before 2022-07-20] what license is it bound to?

The previous license without the Non-Commercial clause; MIT, with GPLv3+
licenses as specified in the software.

The license combination guarantees a non-revocable license, so you can feel
free to use it as you have been, including making derivative works, etc.

## I already copied the software and made a derivative on [date before 2022-07-20], what license applies?

Derivative creation time is key here (see previous question). The previous
license applies; MIT, with GPLv3+ licenses as specified in the software.

## I've been using your software since [date before 2022-07-20]. Can I continue to do so?

The license granted is non-revocable, so yes.

## I'm making a derivative now, after cloning your repo on [date before 2022-07-20]. What license applies?

The newer Non-Commercial license applies to you.

## I can clone your repo and rewind to an earlier commit which makes no mention of the Non-Commercial restriction. Does that mean the previous, more permissive license applies to the software?

No, only the date at which you make the derivative work from the software
matters. As it's not possible to go back in time, the later license applies
now.

## I ran `git pull` and it updated the license, am I now bound to the updated license?

You're bound to the updated license if you're starting to make a derivative work NOW.

## Why do you hate it when corporations profit?

All corporate profit is theft. Furthermore, capitalism is destroying this
planet, and I want no part of it.
