#!/bin/bash
# vi: et sts=4 sw=4 ts=4

# Removes old revisions of snaps
# CLOSE ALL SNAPS BEFORE RUNNING THIS

ask_yn() {
    local -r PROMPT=$1 DEFAULT=$2
	local ANSWER
    read -r -p "$PROMPT [$([[ $DEFAULT = 'y' ]] && echo 'Y/n' || echo 'y/N')]? " ANSWER
    [[ $ANSWER =~ [01NYny] ]] || ANSWER=$DEFAULT
    [[ $ANSWER =~ [1Yy] ]]
}

USAGE() {
    printf 'Usage: %s [-f]...\n' \
        "${0##*/}"
}

HELP_MESSAGE() {
    USAGE
    cat <<EOF
Clean up unused snaps.

  -h        Show this help message.
  -f        Force remove disabled snaps.
  --        Terminate options list.

Copyright (C) 2023-2024 Dan Church.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
with Commons Clause 1.0 (https://commonsclause.com/).
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
You may NOT use this software for commercial purposes.
EOF
}

OPT_FORCE=0
while getopts 'hf' FLAG; do
    case "$FLAG" in
        'f')
            OPT_FORCE=1
            ;;
        'h')
            HELP_MESSAGE
            exit 0
            ;;
        *)
            printf 'Unrecognized flag: %s\n' \
                "$FLAG" \
                >&2
            USAGE >&2
            exit 2
            ;;
    esac
done

shift "$((OPTIND-1))"

# TODO This script sometimes needs to be run multiple times:
# $ cleanup-snaps.sh
# code (revision 151) removed
# $ cleanup-snaps.sh
# core20 (revision 2105) removed
# snapd (revision 20290) removed
# $ cleanup-snaps.sh
# core22 (revision 1033) removed
#
# I'm guessing there's a hierarchy to snaps where they depend on one another.
# Seems like this needs a recursive mode.

disabled_snaps() {
    LC_MESSAGES=en_US \
    snap list --all |
    while read -r SNAPNAME _ REVISION _ _ NOTES; do
        if [[ $NOTES = *disabled* ]]; then
            printf -- '%s %s\n' "$SNAPNAME" "$REVISION"
        fi
    done
}

cleanup_disabled_snaps() {
    local SNAPNAME REVISION
    while read -u 3 -r SNAPNAME REVISION; do
        if [[ $OPT_FORCE -ne 0 ]] || ask_yn "Remove $SNAPNAME Rev:$REVISION" y; then
            sudo snap remove "$SNAPNAME" --revision="$REVISION"
        fi
    done 3< <(disabled_snaps)
}

cleanup_disabled_snaps
if [[ -n $(disabled_snaps) ]]; then
    printf 'Warning: There are more disabled snaps. Run again to remove them.\n' \
        >&2
fi
